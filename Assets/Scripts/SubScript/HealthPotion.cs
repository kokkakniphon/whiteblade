﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().RecievePotion();
            this.gameObject.GetComponent<SoundAssetPlayer>().Play();
            Destroy(this.gameObject);
        }    
    }
}
