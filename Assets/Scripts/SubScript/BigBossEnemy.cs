﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using TMPro;

public class BigBossEnemy : CharacterStats
{
    public TextMeshProUGUI bossName;

    [Header("General Setting")]
    public Transform this_lockOnTarget;
    public GameObject currentCanvas;


    [Header("Movement Setting")]
    public float lookAngle = 45f;
    public float lookRadius = 10f;
    public float rotationSpeed = 5f;

    [Header("Attack Setting")]
    public float attackDelay = 2f;
    float attackCurrentCooldown;
    public float attackCurrentDistanceCooldown;
    public bool foundPlayer;
    public bool attackOnceDelay;
    float distance;

    [Header("Phases Setting")]
    private bool firstPhaseStarted = false;
    private bool secondPhaseStarted = false;
    private bool thirdPhaseStarted = false;
    private bool summonerPhaseOneDone = false;
    private bool summonerPhaseTwoDone = false;
    public GameObject lastTarget;
    public GameObject minionSpinner;
    public GameObject minionMelee;
    public GameObject minionRange;
    public List<EnemyCore> allMinion = new List<EnemyCore>(); 
    public Transform[] minionSpawnPos;

    [Header("Range Attack Setting")]
    public GameObject bulletPrefab;
    public float bulletInitialForce;
    public GameObject gunBarrel;
    public float longRangeAttackCoolDown;
    float currentRangeCoolDown;
    public Vector3 bulletOffset;
    public float bulletDamage;

    Transform target;
    [HideInInspector] public Vector3 lastSeenTarget;
    [HideInInspector] public Animator anim;
 
    [Header("Attack Moveset")]
    public Transform roomCenter;
    public Transform hideBoss;
    public int currentPhase;
    public int currentSubPhase;
    [SerializeField] private float currentAnimationDuration = -1;
    [SerializeField] private List<float> allAttackAnimationDuration = new List<float>();
    public int rangeAttackNum;
    public GameObject bossAoEAttack;
    public GameObject bossGroundAttack;
    public float aoeDamage;
    public float groundDamage;
    bool teleportedBack;

    [Header("Sound Setting")]
    public AudioSource bossTheme;
    public AudioSource shootingSound;
    public AudioSource aoeSound;
    
    public override void Start() 
    {
        base.Start();
        anim = GetComponent<Animator>();

        currentPhase = 1;
        currentSubPhase = 1;
        takedDamage += GetHitted;

        GetAttacksDuration();
    }

    private void OnDestroy() 
    {
        takedDamage -= GetHitted;
    }

    public void GetAttacksDuration()
    {
        allAttackAnimationDuration.Clear();
        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;
        for (int i = 0; i < clips.Length; i++)
        {
            string attackName = "Attack" + i.ToString();
            for (int j = 0; j < clips.Length; j++)
            {
                if(clips[j].name == attackName)
                {
                    allAttackAnimationDuration.Add(clips[j].length);
                }
            }
        }
    }

    public override void Update()
    {
        if(GM.instance.player == null)
            return;

        
        base.Update();
        if(!foundPlayer)
        {
            healthBar.SetActive(false);
            bossName.gameObject.SetActive(false);
            distance = Vector3.Distance(GM.instance.player.transform.position, transform.position);

            if (distance <= lookRadius || foundPlayer)
            {
                float angle = Mathf.Acos(Vector3.Dot((GM.instance.player.transform.position - transform.position).normalized, transform.forward)) * Mathf.Rad2Deg;
                if(angle < lookAngle || foundPlayer)
                {   
                    if (!foundPlayer)
                    {
                        foundPlayer = true;
                        bossTheme.Play();
                        healthBar.SetActive(true);
                        bossName.gameObject.SetActive(true);
                    }
                }
            }
        }

        if(!isDead && foundPlayer)
        {
            if(currentCanvas.activeInHierarchy == Pause.GamePause)
                currentCanvas.SetActive(!Pause.GamePause);
            if(attackCurrentCooldown > 0)
                attackCurrentCooldown -= Time.deltaTime;

            if(currentHealth > 2/3f*maxHealth)
            {
                currentPhase = 1;
            }
            else if (currentHealth > 1/3f*maxHealth)
            {
                currentPhase = 2;
            }
            else
            {
                currentPhase = 3;
            }

            if(currentPhase == 1)
            {
                FirstPhase();
            }
            else if(currentPhase == 2)
            {
                SecondPhase();
            }
            else if(currentPhase == 3)
            {
                ThirdPhase();
            }

            if(currentRangeCoolDown > 0)
                currentRangeCoolDown -= Time.deltaTime;
            FaceTarget();
        }
    }

    void FaceTarget()
    {
        // if(!isAttacking)
        // {
            Vector3 direction = (GM.instance.player.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        // }
    }
    
    void SpawnMinion(GameObject minionType, Vector3 targetPos)
    {
        EnemyCore enemy = Instantiate(minionType, targetPos,Quaternion.identity).GetComponent<EnemyCore>();
        enemy.foundPlayer = true;
        allMinion.Add(enemy);
    }

    IEnumerator RangeAttack()
    {
        anim.SetFloat("S_Attack_Set", 0);
        anim.SetTrigger("S_Attack");
        isAttacking = true;
        rangeAttackNum--;
        shootingSound.Play();
        yield return new WaitForSeconds(allAttackAnimationDuration[0]);
        isAttacking = false;
    }

    IEnumerator TeleportAttack(int secondMove, int nextPhaseNum)
    {
        anim.SetFloat("S_Attack_Set", 3);
        anim.SetTrigger("S_Attack");
        isAttacking = true;
        yield return new WaitForSeconds(allAttackAnimationDuration[3]);
        anim.SetFloat("S_Attack_Set", secondMove);
        anim.SetTrigger("S_Attack");
        if(secondMove == 1)
            aoeSound.Play();
        yield return new WaitForSeconds(allAttackAnimationDuration[secondMove]);
        isAttacking = false;
        currentSubPhase = nextPhaseNum;
        rangeAttackNum = 2;
    }

    public void Shoot()
    {
        Rigidbody bulletRD = Instantiate(bulletPrefab, gunBarrel.transform.position, Quaternion.identity).GetComponent<Rigidbody>();
        bulletRD.gameObject.GetComponent<Bullet>().SetStat(bulletDamage,true,GM.instance.player.transform.gameObject.tag,this.tag,true);
        bulletRD.AddForce(((GM.instance.player.transform.position - bulletRD.transform.position) + bulletOffset).normalized * bulletInitialForce);
        currentRangeCoolDown = longRangeAttackCoolDown;
    }

    public void TeleportToTarget()
    {
        if(currentSubPhase == 2)
        {
            this.transform.position = GM.instance.player.transform.position;
        }
        else if(currentSubPhase == 3 || currentSubPhase == 1)
        {
            this.transform.position = roomCenter.position;
        }
        else if(currentSubPhase == 0)
        {
            this.transform.position = hideBoss.position;
        }
    }

    public void AoeAttack()
    {
        GameObject aoeFX = Instantiate(bossAoEAttack, transform.position, Quaternion.identity);
        aoeFX.GetComponent<ExplosionArea>().SetStat(aoeDamage,true,GM.instance.player.transform.gameObject.tag,this.tag,15f);
        Destroy(aoeFX,3f);
    }

    public void GroundAttack()
    {
        for(int i = 0; i < Random.Range(10,15); i++)
        {
            GameObject aoeFX = Instantiate(bossGroundAttack, GM.instance.player.transform.position + new Vector3(Random.Range(-10,10),0.5f,Random.Range(-10,10)), Quaternion.identity);
            aoeFX.GetComponent<DelayGroundExplose>().SetStat(groundDamage,true,GM.instance.player.transform.gameObject.tag,this.tag,true);
            Destroy(aoeFX,3f);
        }
    }

    void OnFirstPhaseStart()
    {
        if(!firstPhaseStarted)
        {
            firstPhaseStarted = true;

            rangeAttackNum = 2;
        }
    }

    void OnSecondPhaseStarting()
    {
        if(!secondPhaseStarted)
        {
            secondPhaseStarted = true;
            StopAllCoroutines();
            StartCoroutine(OnSecondPhaseStart());
        }
    }

    IEnumerator OnSecondPhaseStart()
    {
        teleportedBack = false;
        rangeAttackNum = 2;
        isAttacking = false;
        currentSubPhase = 0;
        anim.StopPlayback();
        anim.SetFloat("S_Attack_Set", 3);
        anim.SetTrigger("S_Attack");
        summonerPhaseOneDone = true;
        yield return new WaitForSeconds(allAttackAnimationDuration[3]);
        for (int i = 0; i < 10; i++)
        {
            if(i < minionSpawnPos.Length)
            {
                if(i < 4)
                    SpawnMinion(minionRange,minionSpawnPos[i].position);
                else
                    SpawnMinion(minionMelee,minionSpawnPos[i].position);
            }
        }
        summonerPhaseOneDone = false;
    }

    void OnThirdPhaseStarting()
    {
        if(!thirdPhaseStarted)
        {
            thirdPhaseStarted = true;
            StopAllCoroutines();
            StartCoroutine(OnThirdPhaseStart());
        }
    }

    IEnumerator OnThirdPhaseStart()
    {
        teleportedBack = false;
        rangeAttackNum = 2;
        isAttacking = false;
        currentSubPhase = 0;
        anim.StopPlayback();
        anim.SetFloat("S_Attack_Set", 3);
        anim.SetTrigger("S_Attack");
        summonerPhaseTwoDone = true;
        yield return new WaitForSeconds(allAttackAnimationDuration[3]);
        for (int i = 0; i < 12; i++)
        {
            if(i < minionSpawnPos.Length)
            {
                if(i < 3)
                    SpawnMinion(minionSpinner,minionSpawnPos[i].position);
                else if(i < 8)
                    SpawnMinion(minionMelee,minionSpawnPos[i].position);
                else
                    SpawnMinion(minionRange,minionSpawnPos[i].position);
            }
        }
        summonerPhaseTwoDone = false;
    }

    IEnumerator TeleportToTargetWithDelay()
    {
        anim.SetFloat("S_Attack_Set", 3);
        anim.SetTrigger("S_Attack");
        yield return new WaitForSeconds(allAttackAnimationDuration[3]);
        teleportedBack = true;
    }

    public void FirstPhase()
    {
        OnFirstPhaseStart();
        if(currentSubPhase == 1)
        {
            if(currentRangeCoolDown <= 0 && !isAttacking)
            {
                StartCoroutine(RangeAttack());
            }
            if(rangeAttackNum == 0)
            {
                currentSubPhase = 2;
            }
        }
        else if(currentSubPhase == 2)
        {
            if(!isAttacking)
            {
                StartCoroutine(TeleportAttack(1,1));
            }
        }

    }

    public void SecondPhase()
    {
        OnSecondPhaseStarting();
        
        for (int i = 0; i < allMinion.Count; i++)
        {
            if(allMinion[i] == null)
            {
                allMinion.RemoveAt(i);
            }
        }
        if(allMinion.Count <= 0 && !summonerPhaseOneDone)
        {
            summonerPhaseOneDone = true;
            currentSubPhase = 1;
            StartCoroutine(TeleportToTargetWithDelay());
        }

        if(summonerPhaseOneDone && teleportedBack)
        {
            if(currentSubPhase == 1)
            {
                if(currentRangeCoolDown <= 0 && !isAttacking)
                {
                    StartCoroutine(RangeAttack());
                }
                if(rangeAttackNum == 0)
                {
                    currentSubPhase = 2;
                }
            }
            else if(currentSubPhase == 2)
            {
                if(!isAttacking)
                {
                    StartCoroutine(TeleportAttack(1,3));
                }
            }
            else if(currentSubPhase == 3)
            {
                if(!isAttacking)
                {
                    StartCoroutine(TeleportAttack(2,1));
                }
            }
        }

    }

    public void ThirdPhase()
    {
        OnThirdPhaseStarting();

        for (int i = 0; i < allMinion.Count; i++)
        {
            if(allMinion[i] == null)
            {
                allMinion.RemoveAt(i);
            }
        }
        if(allMinion.Count <= 0 && !summonerPhaseTwoDone)
        {
            summonerPhaseTwoDone = true;
            currentSubPhase = 1;
            StartCoroutine(TeleportToTargetWithDelay());
            anim.speed = 1.5f;
            longRangeAttackCoolDown = 0;
            GetAttacksDuration();
        }

        if(summonerPhaseTwoDone)
        {
            if(currentSubPhase == 1)
            {
                if(currentRangeCoolDown <= 0 && !isAttacking)
                {
                    StartCoroutine(RangeAttack());
                }
                if(rangeAttackNum == 0)
                {
                    currentSubPhase = 2;
                }
            }
            else if(currentSubPhase == 2)
            {
                if(!isAttacking)
                {
                    StartCoroutine(TeleportAttack(1,3));
                }
            }
            else if(currentSubPhase == 3)
            {
                if(!isAttacking)
                {
                    StartCoroutine(TeleportAttack(2,1));
                }
            }
        }

    }
    public override void Die()
    {
        base.Die();
        anim.SetTrigger("C_Dead");
        GM.instance.BossDead();
    }

    void GetHitted(GameObject obj)
    {
        if (!foundPlayer && obj == this.gameObject)
        {
            foundPlayer = true;
            bossTheme.Play();
            healthBar.SetActive(true);
            bossName.gameObject.SetActive(true);
        }
    }
}
