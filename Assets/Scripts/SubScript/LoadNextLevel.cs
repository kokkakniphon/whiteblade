﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadNextLevel : MonoBehaviour
{
    public GameObject lookAtDoorCam;
    public Vector2 goInDir;
    private void Awake() 
    {
        lookAtDoorCam.SetActive(false);    
    }
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Player")
        {
            lookAtDoorCam.SetActive(true);
            CameraController.instance.ChangeLevel();
            GM.instance.ChangeToNextLevel();
            other.gameObject.GetComponent<PlayerController>().ForceInput(goInDir);
        }    
    }
}
