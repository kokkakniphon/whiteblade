﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerEnterRoom : MonoBehaviour
{
    public Room thisRoom;
    private void OnTriggerEnter(Collider other) 
    {
        if(other.gameObject.tag == "Player" && !thisRoom.isClear)
        {
            thisRoom.PlayerEnterRoom();
        }    
    }
}
