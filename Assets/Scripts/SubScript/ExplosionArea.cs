﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ExplosionArea : MonoBehaviour
{

    private float damage;
    private string targetTag;
    private bool isAttacking;
    private string thisTag;
    public float effectArea;
    public AnimationCurve damageOverDistance;

    public delegate void noArgs();
    public noArgs hitted;

    public void SetStat(float setDamage, bool setAttack, string setTargetTag, string setThisTag, float setAoeArea)
    {
        damage = setDamage;
        isAttacking = setAttack;
        targetTag = setTargetTag;
        thisTag = setThisTag;
    }

    private void Start() 
    {
        AreaDamageEnemies(this.transform.position, effectArea, damage);
        this.GetComponent<CinemachineImpulseSource>().GenerateImpulse();
    }

    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, effectArea);    
    }
    void AreaDamageEnemies(Vector3 location, float radius, float damage)
    {
        Collider[] objectsInRange = Physics.OverlapSphere(location, radius);
        foreach (Collider col in objectsInRange)
        {
            CharacterStats enemy = col.GetComponent<CharacterStats>();
            if (enemy != null && enemy.tag == targetTag)
            {
                // linear falloff of effect
                float proximity = (location - enemy.transform.position).magnitude;
                float effect = 1 - (proximity / radius);

                enemy.TakeDamage(damage - (damage * damageOverDistance.Evaluate(effect)));
                Debug.Log("AOE hitted: " + enemy.name);
            }
        }

    }
}
