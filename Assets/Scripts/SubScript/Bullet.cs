﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    private float damage;
    private string targetTag;
    private bool isAttacking;
    private string thisTag;
    public bool doDamage;
    public SoundAsset hitNormal;
    public SoundAsset hitHead;

    public GameObject partical;

    public delegate void boolArgs(bool isHead, Bullet thisScript);
    public boolArgs hitted;

    public void SetStat(float setDamage, bool setAttack, string setTargetTag, string setThisTag, bool setDoDamage)
    {
        damage = setDamage;
        isAttacking = setAttack;
        targetTag = setTargetTag;
        thisTag = setThisTag;
        doDamage = setDoDamage;
    }

    private void OnTriggerEnter(Collider other) 
    {
        if (other.gameObject.tag != thisTag && !other.isTrigger)
        {
            GameObject par = Instantiate(partical, this.transform.position, Quaternion.identity);
            if(!doDamage)
            {
                par.GetComponent<ExplosionArea>().SetStat(damage,true,targetTag,thisTag,5f);
            }
            Destroy(par, 5f);
		
            Destroy(this.gameObject);
		
        }
        if(other.gameObject.tag == targetTag)
        {
            CharacterStats stat = other.gameObject.GetComponent<CharacterStats>();
            if(other.gameObject.name == "Head")
            {
                if(hitted != null)
                    hitted.Invoke(true, this);
                stat = other.gameObject.GetComponent<HeadCollider>().thisParentStats;
                if(stat.gotHitHeadSFX != null)
                    this.GetComponent<SoundAssetPlayer>().Play(stat.gotHitHeadSFX);
                damage *= 2f;
            }
            else
            {
                if(hitted != null)
                    hitted.Invoke(false, this);
                if(stat.gotHitBodySFX != null)
                    this.GetComponent<SoundAssetPlayer>().Play(stat.gotHitBodySFX);
            }
            if(doDamage)
                stat.TakeDamage(damage);
            Animator anim = other.gameObject.GetComponent<Animator>();
        }
    }
}
