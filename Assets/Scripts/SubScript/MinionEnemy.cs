﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class MinionEnemy : EnemyCore
{
    Animator animator;

    [Header("Shooting Setting")]
    public GameObject bulletOBJ;
    [SerializeField] float shootingRate = 0.2f;
    float currentShootingCoolDown;
    [SerializeField] float bulletInitialSpeed = 10f;
    [SerializeField] Vector3 bulletOffset;
    [SerializeField] float bulletDamage;
    [Header("Drop Health Potion")]
    public GameObject healthPotion;
    [Range(0,1)]
    public float dropRate = 0.3f;

    private void Awake() 
    {
        animator = GetComponent<Animator>();    
    }

    public override void Start()
    {
        base.Start();
    }

    public override void OnDestroy() 
    {
        base.OnDestroy();
    }

    void Shoot()
    {
        Rigidbody bulletRD = Instantiate(bulletOBJ, currentWeapon.gameObject.transform.position, currentWeapon.gameObject.transform.rotation).GetComponent<Rigidbody>();
        bulletRD.gameObject.GetComponent<Bullet>().SetStat(bulletDamage, true, "Player", this.tag,true);
        Vector3 targetPos = GM.instance.player.transform.transform.position - bulletRD.transform.position;
        targetPos.y = 0;
        bulletRD.AddForce(targetPos.normalized * bulletInitialSpeed);

        Destroy(bulletRD.gameObject, 3f);
    }

    public override void Attack()
    {
        currentPhase = 0;

        if (enemyType == EnemyType.normalClose || enemyType == EnemyType.spinClose)
        {
            currentWeapon.SetTargetTag("Player");
            currentWeapon.SetDamage(damage.GetValue());
        }

        animator.SetTrigger("S_Attack");
        if (enemyType == EnemyType.normalFar)
        {
            Shoot();
        }
        base.Attack();
    }

    public override void Die()
    {
        if(Random.Range(0,1) < dropRate)
        {
            Instantiate(healthPotion, this.transform.position, Quaternion.identity);
        }
        if(foundPlayer)
            base.Die();
    }

    public override void startAttacking()
    {
        base.startAttacking();
    }

    public override void endAttacking()
    {
        base.endAttacking();
    }

    public override void startInAttackArea()
    {
        base.startInAttackArea();
    }

    public override void endInAttackArea()
    {
        base.endInAttackArea();
    }
}
