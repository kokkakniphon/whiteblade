﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayGroundExplose : MonoBehaviour
{
    private float damage;
    private string targetTag;
    private bool isAttacking;
    private string thisTag;
    public bool doDamage;

    public GameObject partical;

    public void SetStat(float setDamage, bool setAttack, string setTargetTag, string setThisTag, bool setDoDamage)
    {
        damage = setDamage;
        isAttacking = setAttack;
        targetTag = setTargetTag;
        thisTag = setThisTag;
        doDamage = setDoDamage;
    }
    private void Start() 
    {
        StartCoroutine(startExplose());
    }

    IEnumerator startExplose()
    {
        yield return new WaitForSeconds(1f);
        GameObject par = Instantiate(partical, this.transform.position, Quaternion.identity);
        par.GetComponent<ExplosionArea>().SetStat(damage,true,targetTag,thisTag,5f);
        Destroy(par, 2f);
    }
}
