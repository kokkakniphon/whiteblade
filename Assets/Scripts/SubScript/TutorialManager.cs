﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public List<GameObject> tutorials;

    private void Start() 
    {
        GM.instance.isChangingLevel = false;
    }

    public void ShowSpecificTutorial(int num)
    {
        for (int i = 0; i < tutorials.Count; i++)
        {
            if(num != i)
            {
                tutorials[i].SetActive(false);
            }
            else if(!tutorials[i].activeInHierarchy)
            {
                tutorials[i].SetActive(true);
                this.GetComponent<SoundAssetPlayer>().Play();
            }
        }
    }
}
