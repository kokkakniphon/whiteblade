﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class BossEnemyTroll : EnemyCore
{
    public TextMeshProUGUI bossName;
public List<EnemyCore> minions = new List<EnemyCore>();
	public bool bossThemePlayed = false;
    public override void Start()
    {
        base.Start();
        GM.instance.player.GetComponent<PlayerController>().usedSkill += Die;
    }

    public override void OnDestroy() 
    {
        base.OnDestroy();
        GM.instance.player.GetComponent<PlayerController>().usedSkill -= Die;
    }

    public override void Attack()
    {
        currentPhase = (currentHealth < maxHealth/2) ? 1 : 0;

        currentWeapon.SetTargetTag("Player");
        currentWeapon.SetDamage(damage.GetValue());

        base.Attack();
    }

    public override void Update()
    {
        base.Update();
        anim.SetBool("isAttacking",isAttacking);

        if(foundPlayer)
        {
            bossName.gameObject.SetActive(true);
            if(healthBar != null)
                healthBar.SetActive(true);
		for(int i = 0; i < minions.Count; i++)
		{
			if(!minions[i].foundPlayer)
				minions[i].SetFoundPlayer(true);
		}
		if(!bossThemePlayed)
		{
			bossThemePlayed = true;
			this.GetComponent<AudioSource>().Play();
		}
        }
        else
        {
            bossName.gameObject.SetActive(false);
            if(healthBar != null)
                healthBar.SetActive(false);
        }
    }

    public override void Movement()
    {
        if(!isDead)
        {
            base.Movement();
            if(isAttacking && attackCurrentDistanceCooldown > 0 && !isAttackClose && anim.GetFloat("S_Attack_Set") >= 3)
            {
                agent.Move((lastSeenTarget - transform.position).normalized * 20f * Time.deltaTime);
                if(Vector3.Distance(lastSeenTarget , transform.position) <= agent.stoppingDistance)
                {
                    base.endAttacking();
                    attackOnceDelay = false;
                }
            }
            SceneManager.LoadScene("YouWin");
        }
    }

    public override void Die()
    {
        if(foundPlayer)
	{
            base.Die();
		
	}
    }

    public override void startAttacking()
    {
        base.startAttacking();
    }

    public override void endAttacking()
    {
        base.endAttacking();
    }

    public override void startInAttackArea()
    {
        base.startInAttackArea();
    }

    public override void endInAttackArea()
    {
        base.endInAttackArea();
    }
}
