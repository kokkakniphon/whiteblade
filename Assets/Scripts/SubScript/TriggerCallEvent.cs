﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerCallEvent : MonoBehaviour
{
    public UnityEvent onTriggerEnterEvent;
    private void OnTriggerEnter(Collider other) 
    {
        if(onTriggerEnterEvent != null && other.gameObject.tag == "Player")
            onTriggerEnterEvent.Invoke();
    }
}
