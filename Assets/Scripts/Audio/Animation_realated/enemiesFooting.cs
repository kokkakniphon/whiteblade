﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemiesFooting : MonoBehaviour
{
    [SerializeField]
    public AudioClip[] steps;
    public AudioClip[] runs;
    public AudioClip roll;
    public AudioSource audioSource;
    float runCoolDown;
    float walkCoolDown;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Step()
    {
        if (walkCoolDown <= 0)
        {
            AudioClip clip = GetRandomSteps();
            audioSource.PlayOneShot(clip);
            walkCoolDown = 0.5f;
        }
    }

    public AudioClip GetRandomSteps()
    {
        return steps[UnityEngine.Random.Range(0, steps.Length)];
    }

    public void Run()
    {
        if (runCoolDown <= 0)
        {
            AudioClip clip = GetRandomRuns();
            audioSource.PlayOneShot(clip);
            runCoolDown = 0.3f;
        }
    }

    public AudioClip GetRandomRuns()
    {
        return runs[UnityEngine.Random.Range(0, runs.Length)];
    }

    public void Roll()
    {
        AudioClip clip = roll;
        audioSource.PlayOneShot(clip);
    }

    private void Update()
    {
        if (walkCoolDown > 0)
            walkCoolDown -= Time.deltaTime;
        if (runCoolDown > 0)
            runCoolDown -= Time.deltaTime;
    }
}
