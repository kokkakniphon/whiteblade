﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour
{
        [SerializeField]
    public AudioClip[] swings;
    public AudioSource audioSource;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void Swing()
    {
        
        AudioClip clip = GetRandomSwings();
        audioSource.PlayOneShot(clip);
            
        Debug.Log("swing");
        
    }

    public AudioClip GetRandomSwings()
    {
        return swings[UnityEngine.Random.Range(0, swings.Length)];
    }
}
