﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;
public class SetVolume : MonoBehaviour
{
    public Slider masterSlider;
    public Slider musicSlider;
    public Slider sfxSlider;
    public Slider mouseSlider;

    private void Awake() 
    {
        GM.onFinishLoading += Start;
    }

    private void OnDestroy() 
    {
        GM.onFinishLoading -= Start;    
    }

    public void Start() 
    {
        masterSlider.value = PlayerPrefs.GetFloat("MasterVolume");

        musicSlider.value = PlayerPrefs.GetFloat("MusicVolume");

        sfxSlider.value = PlayerPrefs.GetFloat("SfxVolume");

        mouseSlider.value = PlayerPrefs.GetFloat("MouseSensitivity");
    }

    public AudioMixer mixer;
    public void SetMaster(float SliderValue) 
    {
        PlayerPrefs.SetFloat("MasterVolume", SliderValue);    
        mixer.SetFloat("MasterVolume", Mathf.Log10(SliderValue)*20);
    }
    public void SetMusic(float SliderValue)
    {
        PlayerPrefs.SetFloat("MusicVolume", SliderValue);    
        mixer.SetFloat("MusicVolume", Mathf.Log10(SliderValue) * 20);
    }
    public void SetSfx(float SliderValue)
    {
        PlayerPrefs.SetFloat("SfxVolume", SliderValue);  
        mixer.SetFloat("SfxVolume", Mathf.Log10(SliderValue) * 20);
    }
    public void SetMouse(float SliderValue)
    {
        PlayerPrefs.SetFloat("MouseSensitivity", SliderValue); 
        GM.instance.mouseSensitivity = SliderValue;
    }
}
