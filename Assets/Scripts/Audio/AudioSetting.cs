﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioSetting : MonoBehaviour
{
    public AudioMixer audioMixer;
    public void SetMaster(float volume) {
        audioMixer.SetFloat("MasterVolume", volume);
    }
}
