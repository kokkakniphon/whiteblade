﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LevelController : MonoBehaviour
{
    [Header("Level Setting")]
    [Tooltip("The max size of the level.")]
    public Vector2 levelDimension;
    [Tooltip("The max size of the room.")]
    public Vector2 gridDimension;
    public int numberOfRooms = 8;
    int currentNumberOfRooms;
    int gridSizeX, gridSizeY;
    [Header("Setup Room Prefabs")]
    public List<GameObject> allSpawnRoom;
    public List<GameObject> allRooms;
    public List<GameObject> allEndRooms;
    public List<GameObject> allNextLevelRoom;
    List<Room> currentAllRoom = new List<Room>();
    Room[,] rooms;
    List<Vector2> takenPositions = new List<Vector2>();
    // This will use to make sure the room will connect
    List<Vector2> notConnectedPositions = new List<Vector2>();
    // This will use to connect corridor
    List<Vector4> notConnectedRoom = new List<Vector4>();
    int currentRoomX, currentRoomY;
    int notConnectedDoorNum;
    // Top Down Left Right
    Vector4 notConnectDoor;
    public void StartGenerate() 
    {
        currentNumberOfRooms = numberOfRooms;
        gridSizeX = Mathf.RoundToInt(gridDimension.x);
        gridSizeY = Mathf.RoundToInt(gridDimension.y);
        SetUpSpawnRoom();
        GenerateRoom();
        ExitRoom();
        for (int i = 0; i < 100; i++)
        {
            float verticalCheck = notConnectDoor.x - notConnectDoor.y;
            float horizontalCheck = notConnectDoor.z - notConnectDoor.w;
            if(verticalCheck == 0 && horizontalCheck == 0 && currentNumberOfRooms <= 0 && notConnectedDoorNum == 0)
            {
                break;
            }
            Reset();
        }

        for (int i = 0; i < currentAllRoom.Count; i++)
        {
            if(currentAllRoom[i] != null)
            {
                for (int j = 0; j < currentAllRoom[i].doors.Count; j++)
                {
                    if(currentAllRoom[i].doors[j].connectedTo == null)
                    {
                        Reset();
                    }
                }
            }
        }

        transform.position = Vector3.zero;
        GetComponent<NavMeshSurface>().BuildNavMesh();

	    for (int i = 0; i < currentAllRoom.Count; i++)
        {
            if(currentAllRoom[i] != null)
            {
                currentAllRoom[i].DecoraTheRoom();
                currentAllRoom[i].PlaceEnemy();
            }
        }
    }

    public void Reset() 
    {
        for (int i = 0; i < currentAllRoom.Count; i++)
        {
            if(currentAllRoom[i] != null)
                Destroy(currentAllRoom[i].gameObject);
        }

        currentAllRoom.Clear();
        takenPositions.Clear();
        notConnectedPositions.Clear();
        notConnectedRoom.Clear();
        notConnectedDoorNum = 0;
        notConnectDoor = Vector4.zero;
        StartGenerate();
    }

    public void Clear()
    {
        for (int i = 0; i < currentAllRoom.Count; i++)
        {
            if(currentAllRoom[i] != null)
                Destroy(currentAllRoom[i].gameObject);
        }

        currentAllRoom.Clear();
        takenPositions.Clear();
        notConnectedPositions.Clear();
        notConnectedRoom.Clear();
        notConnectedDoorNum = 0;
        notConnectDoor = Vector4.zero;
        GetComponent<NavMeshSurface>().RemoveData();
    }

    void SetUpSpawnRoom()
    {
        // Generate Spawn Room.
        currentRoomX = Mathf.RoundToInt(levelDimension.x/2);
        currentRoomY = Mathf.RoundToInt(levelDimension.y/2);
        rooms = new Room[Mathf.RoundToInt(levelDimension.x+2),Mathf.RoundToInt(levelDimension.y+2)];
        transform.position = new Vector3(currentRoomX*gridSizeX, 0, currentRoomY*gridSizeY);
        rooms[currentRoomX,currentRoomY] = Instantiate(allSpawnRoom[Random.Range(0,allSpawnRoom.Count)], new Vector3(currentRoomX*gridSizeX, 0, currentRoomY*gridSizeY), Quaternion.identity, transform).GetComponent<Room>();
        currentAllRoom.Add(rooms[currentRoomX, currentRoomY]);
        notConnectedPositions.Add(new Vector2(currentRoomX,currentRoomY));
        notConnectedDoorNum = rooms[currentRoomX,currentRoomY].doors.Count;
        takenPositions.Add(new Vector2(currentRoomX,currentRoomY));
        notConnectDoor += rooms[currentRoomX,currentRoomY].GetAllDoors();
        rooms[currentRoomX, currentRoomY].roomPos = new Vector2(currentRoomX, currentRoomY);
    }

    public void CheckAllRooms()
    {
        for (int i = 0; i < takenPositions.Count; i++)
        {
            notConnectedPositions.Add(takenPositions[i]);
        }

        currentRoomX = Mathf.RoundToInt(notConnectedPositions[0].x);
        currentRoomY = Mathf.RoundToInt(notConnectedPositions[0].y);

        GenerateRoom();
    }

    public void GenerateRoom()
    {
        if(notConnectedPositions.Count <= 0 || currentNumberOfRooms <= 0)
            return;

        bool passCheck = true;

        for (int i = 0; i < rooms[currentRoomX, currentRoomY].doors.Count; i++)
        {
            Vector2 nextPos = Vector2.zero;
            switch (rooms[currentRoomX, currentRoomY].doors[i].doorDirection)
            {
                case DoorDirection.bottom:
                    nextPos = new Vector2(currentRoomX,currentRoomY-1);
                    break;
                case DoorDirection.left:
                    nextPos = new Vector2(currentRoomX-1,currentRoomY);
                    break;
                case DoorDirection.top:
                    nextPos = new Vector2(currentRoomX,currentRoomY+1);
                    break;
                case DoorDirection.right:
                    nextPos = new Vector2(currentRoomX+1,currentRoomY);
                    break;
            }

            // Debug.Log("Opposite of door is: " + oppositeDir);

            for (int p = 0; p < takenPositions.Count; p++)
            {
                if( rooms[currentRoomX, currentRoomY].connectedFrom != null)
                {
                    if(takenPositions[p] == nextPos && takenPositions[p] != rooms[currentRoomX, currentRoomY].connectedFrom.roomPos)
                    {
                        passCheck = false;
                        Debug.Log("It's on top of other room from: " + currentRoomX + " , " + currentRoomY + " : " + nextPos);
                    }
                }
            }
        }

        if(passCheck)
        {

            for (int i = 0; i < rooms[currentRoomX, currentRoomY].doors.Count; i++)
            {
                // Debug.Log("Current " + rooms[currentRoomX,currentRoomY].gameObject.name + " doors : " + rooms[currentRoomX, currentRoomY].doors[i].doorDirection);
                if(rooms[currentRoomX, currentRoomY].doors[i].connectedTo == null)
                {
                    DoorDirection oppositeDir = rooms[currentRoomX, currentRoomY].doors[i].doorDirection;
                    switch (oppositeDir)
                    {
                        case DoorDirection.bottom:
                            oppositeDir = DoorDirection.top;
                            break;
                        case DoorDirection.left:
                            oppositeDir = DoorDirection.right;
                            break;
                        case DoorDirection.top:
                            oppositeDir = DoorDirection.bottom;
                            break;
                        case DoorDirection.right:
                            oppositeDir = DoorDirection.left;
                            break;
                    }

                    Vector2 nextPos = Vector2.zero;
                    switch (rooms[currentRoomX, currentRoomY].doors[i].doorDirection)
                    {
                        case DoorDirection.bottom:
                            nextPos = new Vector2(currentRoomX,currentRoomY-1);
                            break;
                        case DoorDirection.left:
                            nextPos = new Vector2(currentRoomX-1,currentRoomY);
                            break;
                        case DoorDirection.top:
                            nextPos = new Vector2(currentRoomX,currentRoomY+1);
                            break;
                        case DoorDirection.right:
                            nextPos = new Vector2(currentRoomX+1,currentRoomY);
                            break;
                    }

                    // Debug.Log("Opposite of door is: " + oppositeDir);

                    Room selectedRoom = null;

                    // Find a next room.
                    List<GameObject> checkRooms = new List<GameObject>();
                    int nextRoomDoor = -1;
                    foreach (GameObject room in allRooms)
                    {
                        if(room != null)
                            checkRooms.Add(room);
                    }
                    for (int j = 0; j < allRooms.Count; j++)
                    {
                        int randomSelectRoom = Random.Range(0,checkRooms.Count);
                        Room selectNextRoom = checkRooms[randomSelectRoom].GetComponent<Room>();
                        for (int k = 0; k < selectNextRoom.doors.Count; k++)
                        {
                            // Debug.Log("Checking door of room " + selectNextRoom.name + " : " + selectNextRoom.doors[k].doorDirection + " to " + oppositeDir);
                            if(selectNextRoom.doors[k].doorDirection == oppositeDir)
                            {
                                selectedRoom = selectNextRoom;
                                nextRoomDoor = k;
                                // Debug.Log("Selected room is: " + selectNextRoom.name);

                                float verticalCheck = (notConnectDoor + selectNextRoom.GetAllDoors()).x - (notConnectDoor + selectNextRoom.GetAllDoors()).y;
                                float horizontalCheck = (notConnectDoor + selectNextRoom.GetAllDoors()).z - (notConnectDoor + selectNextRoom.GetAllDoors()).w;
                                if(selectNextRoom.doors.Count-2 + notConnectedDoorNum > currentNumberOfRooms || verticalCheck == 0 && horizontalCheck == 0 && currentNumberOfRooms-1 > 0)
                                {
                                    selectedRoom = null;
                                }

                                if(selectedRoom != null)
                                    break;
                            }
                        }
                        if(selectedRoom != null)
                            break;
                        
                        checkRooms.RemoveAt(randomSelectRoom);
                        checkRooms.TrimExcess();
                    }

                    if(selectedRoom != null)
                    {
                        int x = Mathf.RoundToInt(nextPos.x);
                        int y = Mathf.RoundToInt(nextPos.y);
                        rooms[x,y] = (Instantiate(selectedRoom,new Vector3(nextPos.x*gridSizeX, 0, nextPos.y*gridSizeY), Quaternion.identity, transform).GetComponent<Room>());
                        rooms[x,y].roomPos = new Vector2(x,y);
                        currentAllRoom.Add(rooms[x,y]);
                        takenPositions.Add(nextPos);
                        notConnectedPositions.Add(new Vector2(x,y));
                        notConnectedRoom.Add(new Vector4(currentRoomX,currentRoomY,x,y));
                        notConnectDoor += rooms[x,y].GetAllDoors();
                        notConnectedDoorNum += rooms[x,y].doors.Count-2;
                        currentNumberOfRooms--;
                        rooms[x, y].doors[nextRoomDoor].isLinked = true;
                        rooms[x, y].doors[nextRoomDoor].connectedTo = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedTo = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedFrom = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedBy = i;
                        rooms[currentRoomX, currentRoomY].doors[i].isLinked = true;
                        rooms[currentRoomX, currentRoomY].doors[i].connectedTo = rooms[x, y];
                        rooms[currentRoomX, currentRoomY].connectedTo = rooms[x, y];
                    }
                    else
                    {
                        Debug.Log("Can't find " + currentRoomX + " , " + currentRoomY);
                    }
                }
            }
        }
        else
        {
            Debug.Log("Remove " + rooms[currentRoomX, currentRoomY].gameObject.name);
            notConnectDoor -= rooms[currentRoomX, currentRoomY].GetAllDoors();
            int newRoomX = (int)rooms[currentRoomX, currentRoomY].connectedFrom.roomPos.x;
            int newRoomY = (int)rooms[currentRoomX, currentRoomY].connectedFrom.roomPos.y;
            rooms[currentRoomX, currentRoomY].connectedFrom.doors[rooms[currentRoomX, currentRoomY].connectedBy].isLinked = false;
            takenPositions.Remove(new Vector2(currentRoomX, currentRoomY));
            notConnectedPositions.Add(new Vector2(newRoomX,newRoomY));
            rooms[currentRoomX, currentRoomY].connectedFrom.doors[rooms[currentRoomX, currentRoomY].connectedBy].connectedTo = null;
            Destroy(rooms[currentRoomX, currentRoomY].gameObject);
            notConnectedPositions.Remove(new Vector2(currentRoomX,currentRoomY));
            if(notConnectedPositions.Count > 0)
            {
                currentRoomX = Mathf.RoundToInt(notConnectedPositions[0].x);
                currentRoomY = Mathf.RoundToInt(notConnectedPositions[0].y);
                GenerateRoom();
            }
            return;
        }

        notConnectedPositions.Remove(new Vector2(currentRoomX,currentRoomY));
        if(notConnectedPositions.Count > 0)
        {
            currentRoomX = Mathf.RoundToInt(notConnectedPositions[0].x);
            currentRoomY = Mathf.RoundToInt(notConnectedPositions[0].y);
            GenerateRoom();
        }
    }

    void ExitRoom()
    {
        for (int i = 0; i < takenPositions.Count; i++)
        {
            currentRoomX = Mathf.RoundToInt(takenPositions[i].x);
            currentRoomY = Mathf.RoundToInt(takenPositions[i].y);
            for (int j = 0; j < rooms[currentRoomX, currentRoomY].doors.Count; j++)
            {
                if (rooms[currentRoomX, currentRoomY].doors[j].connectedTo == null)
                {
                    for (int a = 0; a < rooms[currentRoomX, currentRoomY].doors.Count; a++)
                    {
                        Vector2 nextPosCheck = Vector2.zero;
                        switch (rooms[currentRoomX, currentRoomY].doors[j].doorDirection)
                        {
                            case DoorDirection.bottom:
                                nextPosCheck = new Vector2(currentRoomX,currentRoomY-1);
                                break;
                            case DoorDirection.left:
                                nextPosCheck = new Vector2(currentRoomX-1,currentRoomY);
                                break;
                            case DoorDirection.top:
                                nextPosCheck = new Vector2(currentRoomX,currentRoomY+1);
                                break;
                            case DoorDirection.right:
                                nextPosCheck = new Vector2(currentRoomX+1,currentRoomY);
                                break;
                        }

                        // Debug.Log("Opposite of door is: " + oppositeDir);

                        for (int p = 0; p < takenPositions.Count; p++)
                        {
                            if( rooms[currentRoomX, currentRoomY].connectedFrom != null)
                            {
                                if(takenPositions[p] == nextPosCheck && takenPositions[p] != rooms[currentRoomX, currentRoomY].connectedFrom.roomPos)
                                {
                                    Debug.Log("It's on top of other room from: " + currentRoomX + " , " + currentRoomY + " : " + nextPosCheck);
                                    Reset();
                                    return;
                                }
                            }
                        }
                    }

                    DoorDirection oppositeDir = rooms[currentRoomX, currentRoomY].doors[j].doorDirection;
                    switch (oppositeDir)
                    {
                        case DoorDirection.bottom:
                            oppositeDir = DoorDirection.top;
                            break;
                        case DoorDirection.left:
                            oppositeDir = DoorDirection.right;
                            break;
                        case DoorDirection.top:
                            oppositeDir = DoorDirection.bottom;
                            break;
                        case DoorDirection.right:
                            oppositeDir = DoorDirection.left;
                            break;
                    }

                    Vector2 nextPos = Vector2.zero;
                    switch (rooms[currentRoomX, currentRoomY].doors[j].doorDirection)
                    {
                        case DoorDirection.bottom:
                            nextPos = new Vector2(currentRoomX,currentRoomY-1);
                            break;
                        case DoorDirection.left:
                            nextPos = new Vector2(currentRoomX-1,currentRoomY);
                            break;
                        case DoorDirection.top:
                            nextPos = new Vector2(currentRoomX,currentRoomY+1);
                            break;
                        case DoorDirection.right:
                            nextPos = new Vector2(currentRoomX+1,currentRoomY);
                            break;
                    }

                    Room selectedRoom = null;
                    int nextRoomDoor = -1;

                    // Find a next room.
                    List<GameObject> checkRooms = new List<GameObject>();

                    if(notConnectedDoorNum > 1)
                    {
                        foreach (GameObject room in allEndRooms)
                        {
                            if(room != null)
                                checkRooms.Add(room);
                        }
                    }
                    else
                    {
                        foreach (GameObject room in allNextLevelRoom)
                        {
                            if(room != null)
                                checkRooms.Add(room);
                        }
                    }
                    for (int m = 0; m < allRooms.Count; m++)
                    {
                        int randomSelectRoom = Random.Range(0,checkRooms.Count);
                        Room selectNextRoom = checkRooms[randomSelectRoom].GetComponent<Room>();
                        for (int k = 0; k < selectNextRoom.doors.Count; k++)
                        {
                            // Debug.Log("Checking door of room " + selectNextRoom.name + " : " + selectNextRoom.doors[k].doorDirection + " to " + oppositeDir);
                            if(selectNextRoom.doors[k].doorDirection == oppositeDir)
                            {
                                selectedRoom = selectNextRoom;
                                nextRoomDoor = k;
                            }
                        }
                        if(selectedRoom != null)
                            break;
                        
                        checkRooms.RemoveAt(randomSelectRoom);
                        checkRooms.TrimExcess();
                    }

                    if(selectedRoom != null)
                    {
                        int x = Mathf.RoundToInt(nextPos.x);
                        int y = Mathf.RoundToInt(nextPos.y);
                        rooms[x,y] = (Instantiate(selectedRoom,new Vector3(nextPos.x*gridSizeX, 0, nextPos.y*gridSizeY), Quaternion.identity, transform).GetComponent<Room>());
                        rooms[x,y].roomPos = new Vector2(x,y);
                        takenPositions.Add(nextPos);
                        currentAllRoom.Add(rooms[x, y]);
                        notConnectedPositions.Add(new Vector2(x,y));
                        notConnectedRoom.Add(new Vector4(currentRoomX,currentRoomY,x,y));
                        notConnectDoor += rooms[x,y].GetAllDoors();
                        notConnectedDoorNum += rooms[x,y].doors.Count-2;
                        currentNumberOfRooms--;
                        rooms[x, y].doors[nextRoomDoor].isLinked = true;
                        rooms[x, y].doors[nextRoomDoor].connectedTo = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedTo = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedFrom = rooms[currentRoomX, currentRoomY];
                        rooms[x, y].connectedBy = j;
                        rooms[currentRoomX, currentRoomY].doors[j].isLinked = true;
                        rooms[currentRoomX, currentRoomY].doors[j].connectedTo = rooms[x, y];
                        rooms[currentRoomX, currentRoomY].connectedTo = rooms[x, y];
                    }
                }
            }
        }
    }
}
