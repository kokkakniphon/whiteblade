﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DoorDirection
{
    top,
    left,
    bottom,
    right
}

[System.Serializable]
public class EnemySet
{
    [Range(0,1)]
    public float spawnChance = 1f;
    public List<GameObject> enemySet;
    public List<Transform> allEnemyTransform;
}

[System.Serializable]
public class DirectionSet
{
    [Range(0,1)]
    public float spawnChance = 1f;
    public Decoration decorations;
    public List<Transform> allDecorationsTransform;
}

[System.Serializable]
public class Door
{
    public DoorDirection doorDirection;
    public Animator gate;
    [HideInInspector] public Room connectedTo;
    [HideInInspector] public bool isLinked;
    [HideInInspector] public bool isConnected;

    public void OpenGate(SoundAsset doorSound)
    {
        if(gate != null)
        {
            gate.SetBool("isLock", false);
            SoundAssetPlayer soundAssetPlayer = gate.gameObject.GetComponent<SoundAssetPlayer>();
            soundAssetPlayer.soundAsset = doorSound;
            soundAssetPlayer.PlayAt(gate.gameObject);
        }
    }

    public void LockGate(SoundAsset doorSound)
    {
        if(gate != null)
        {
            gate.SetBool("isLock", true);
            SoundAssetPlayer soundAssetPlayer = gate.gameObject.GetComponent<SoundAssetPlayer>();
            soundAssetPlayer.soundAsset = doorSound;
            soundAssetPlayer.PlayAt(gate.gameObject);
        }
    }
}

public class Room : MonoBehaviour
{
    public List<EnemySet> allEnemySet;
    public List<DirectionSet> allDecorationSet;

    [HideInInspector] public Vector2 roomPos;
    public List<Door> doors;
    public SoundAsset openDoorSound;
    public SoundAsset closeDoorSound;
    public AudioSource minionRoomSound;
    [HideInInspector] public Room connectedTo, connectedFrom;
    [HideInInspector] public int connectedBy;
    [HideInInspector] public List<EnemyCore> allMinion = new List<EnemyCore>();
    [HideInInspector] public int currentMinionAmount;
    [HideInInspector] public bool isClear;
    bool isEntered;

    private void Start() 
    {
        minionRoomSound = this.GetComponent<AudioSource>();    
    }

    public Vector4 GetAllDoors()
    {
        Vector4 allDoors = Vector2.zero;
        foreach (Door door in doors)
        {
            switch (door.doorDirection)
            {
                case DoorDirection.bottom:
                    allDoors.y++;
                    break;
                case DoorDirection.left:
                    allDoors.z++;
                    break;
                case DoorDirection.top:
                    allDoors.x++;
                    break;
                case DoorDirection.right:
                    allDoors.w++;
                    break;
            }
        }
        return allDoors;
    }

    public void DecoraTheRoom()
    {
        foreach (DirectionSet decorateSet in allDecorationSet)
        {
            foreach (Transform decoration in decorateSet.allDecorationsTransform)
            {
                if(decoration != null && decoration.childCount <= 0)
                {
                    if (Random.Range(0,1) < decorateSet.spawnChance && decorateSet.decorations.allDecorations.Count > 0)
                    {
                        Instantiate(decorateSet.decorations.allDecorations[Random.Range(0, decorateSet.decorations.allDecorations.Count)], decoration.position + new Vector3(-2.5f, 0f, 2.5f), decoration.rotation, decoration).transform.localScale *= 2;
                    }
                }
            }
        }
    }

    public void PlaceEnemy()
    {
        foreach (EnemySet enemySetSelection in allEnemySet)
        {
            foreach (Transform enemyTrans in enemySetSelection.allEnemyTransform)
            {
                if(enemyTrans != null && enemyTrans.childCount <= 0)
                {
                    if (Random.Range(0,1) < enemySetSelection.spawnChance && enemySetSelection.enemySet.Count > 0)
                    {
                        currentMinionAmount++;
                        GameObject minion = Instantiate(enemySetSelection.enemySet[Random.Range(0, enemySetSelection.enemySet.Count)], enemyTrans.position + new Vector3(-2.5f, 0, 2.5f), enemyTrans.rotation, enemyTrans);
                        minion.GetComponent<CharacterStats>().triggerDead += MinionGotKill;
                        allMinion.Add(minion.GetComponent<EnemyCore>());
                    }
                }
            }
        }
        // CheckIsClear();
    }

    public void PlayerEnterRoom()
    {
        CheckIsClear();
        if(!isClear && !isEntered)
        {
            isEntered = true;
            foreach (EnemyCore enemy in allMinion)
            {
                enemy.foundPlayer = true;
            }

            foreach (Door door in doors)
            {
                door.LockGate(closeDoorSound);
            }
            StartCoroutine(PlayMinionRoomSound());
        }
    }

    IEnumerator PlayMinionRoomSound()
    {
        float currentValue = minionRoomSound.volume;
        minionRoomSound.Play();
        minionRoomSound.volume = 0f;
        float lerpingValue = (currentValue)/100f;
        for (int i = 0; i < 100f; i++)
        {
            minionRoomSound.volume += lerpingValue;
            yield return new WaitForSeconds(2f/100f);
        }
        minionRoomSound.volume = currentValue;
    }

    IEnumerator StopMinionRoomSound()
    {
        float currentValue = minionRoomSound.volume;
        float lerpingValue = (0-currentValue)/100f;
        for (int i = 0; i < 100f; i++)
        {
            minionRoomSound.volume += lerpingValue;
            yield return new WaitForSeconds(2f/100f);
        }
        minionRoomSound.volume = 0f;
        minionRoomSound.Stop();
    }

    void CheckIsClear()
    {
        if(currentMinionAmount <= 0 && !isClear)
        {
            isClear = true;
            foreach (Door door in doors)
            {
                door.OpenGate(openDoorSound);
            }
            StartCoroutine(StopMinionRoomSound());
        }
    }

    void MinionGotKill(GameObject obj)
    {
        currentMinionAmount--;
        obj.GetComponent<CharacterStats>().triggerDead -= MinionGotKill;
        CheckIsClear();
    }

}
