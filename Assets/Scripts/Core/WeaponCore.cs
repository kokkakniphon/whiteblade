﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponCore : MonoBehaviour
{
    public float damage;
    public string targetTag;
    public bool isAttacking;

    public delegate void noArgs();
    public noArgs hitted;

    public void SetDamage(float setFloat)
    {
        damage = setFloat;
    }

    public void SetAttack(bool setBool)
    {
        isAttacking = setBool;
    }

    public void SetTargetTag(string setString)
    {
        targetTag = setString;
    }

    private void OnTriggerStay(Collider other) 
    {      
        if(isAttacking)
        {
            if(other.gameObject.tag == targetTag)
            {
                if(!other.gameObject.GetComponent<CharacterStats>().hitted)
                {
                    CharacterStats stat = other.gameObject.GetComponent<CharacterStats>();

                    if(other.gameObject.name == "Head")
                    {
                        stat = other.gameObject.GetComponent<HeadCollider>().thisParentStats;
                        if(stat.gotHitHeadSFX != null)
                            this.GetComponent<SoundAssetPlayer>().Play(stat.gotHitHeadSFX);
                        damage *= 2f;
                    }
                    else
                    {
                        if(stat.gotHitBodySFX != null)
                            this.GetComponent<SoundAssetPlayer>().Play(stat.gotHitBodySFX);
                    }

                    stat.TakeDamage(damage);
                    Debug.Log(transform.root.gameObject.name+" is attacking " + other.gameObject.name);
                    Animator anim = other.gameObject.GetComponent<Animator>();
                    anim.SetTrigger("C_Hitted");
                }
            }
        }
    }
}
