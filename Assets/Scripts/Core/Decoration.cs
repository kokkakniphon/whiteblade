﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DECORATIONS", menuName = "Decoration/New Decoration Set", order = 1)]
public class Decoration : ScriptableObject
{
    public List<GameObject> allDecorations;
}
