﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class CameraController : MonoBehaviour
{

    public static CameraController instance;

    [Header("Settings")]
    public CinemachineFreeLook CM_ThirdPerson;
    public CinemachineFreeLook CM_ADSThirdPerson;
    public CinemachineFreeLook CM_LockOnTarget;
    public CinemachineTargetGroup CM_CenterTargetGroup;
    public RectTransform lockOnImage;
    public GameObject[] trackingPosition;

    [Space]
    public bool toggleLockCurser;

    [Header("ADS Setting")]
    public GameObject adsObject;
    [SerializeField] float lockOnSpeedMultiplier = 5f;
    [SerializeField] private float lockAngleRange = 45f;
    [SerializeField] private float lockMinDistance = 45f;
    [SerializeField] private float lockFieldOfViewOffset = 10f;
    public Transform targetTransform;
    [HideInInspector] public bool isLockingOnTarget;
    [HideInInspector] public bool isADSing;
    [SerializeField] float cameraAdjustX = 5f;
    public AnimationCurve zoomEffect;
    float timeEffect;
    public float effectSpeed = 1f;
    bool setADS;
    public bool allowCameraControl = true;
    
    private void Awake() 
    {
        instance = this;

        // Set lock state according to the default setting.
        Cursor.lockState = (toggleLockCurser) ? CursorLockMode.Locked : CursorLockMode.None; 
        Cursor.visible = !toggleLockCurser;   

        // Set default camera position to behind-center of player.
        CM_ThirdPerson.m_XAxis.Value = 0f;
        CM_ThirdPerson.m_YAxis.Value = 0.27f;

        allowCameraControl = true;
    }

    private void Update() 
    {
        if(Pause.GamePause)
            adsObject.SetActive(false);
        if(Pause.GamePause || !allowCameraControl)
            return;

        if(setADS != isADSing)
        {
            setADS = isADSing;
            timeEffect = 0;
        }

        if(adsObject.activeInHierarchy != isADSing)
            adsObject.SetActive(isADSing);

        if(isADSing)
        {
            CM_ThirdPerson.m_XAxis.Value += Input.GetAxis("Mouse X") * GM.instance.mouseSensitivity / (40/25f);
            CM_ThirdPerson.m_YAxis.Value -= Input.GetAxis("Mouse Y") * GM.instance.mouseSensitivity / (50f * (40/25f));

            if(timeEffect < 1)
            {
                timeEffect += Time.deltaTime/effectSpeed;
                CM_ThirdPerson.m_Lens.FieldOfView = Mathf.Lerp(40f,25f,zoomEffect.Evaluate(timeEffect));
            }

        }
        else
        {
            CM_ThirdPerson.m_XAxis.Value += Input.GetAxis("Mouse X") * GM.instance.mouseSensitivity;
            CM_ThirdPerson.m_YAxis.Value -= Input.GetAxis("Mouse Y") * GM.instance.mouseSensitivity / 50f;

            if(timeEffect < 1)
            {
                timeEffect += Time.deltaTime/effectSpeed;
                CM_ThirdPerson.m_Lens.FieldOfView = Mathf.Lerp(25f,40f,zoomEffect.Evaluate(timeEffect));
            }
        }
    }

    public void ChangeLevel()
    {
        CM_ThirdPerson.gameObject.SetActive(false);
    }


    void SelectTargetToLockOn()
    {
        // Find gameobject with the tag "Enemy"
        GameObject[] enemys = GameObject.FindGameObjectsWithTag("Enemy");
        int selectedEnemy = -1;
        float selectedDotResult = -1;

        targetTransform = null;

        // Loop thought list of enemys founded.
        for (int i = 0; i < enemys.Length; i++)
        {
            // Set vector pointing enemy from camera, then get the dot product between camera's forward vector and vecter pointing enemy.
            Vector3 enemyVector = enemys[i].transform.position - Camera.main.transform.position;
            float dotResult = Vector3.Dot(Camera.main.transform.forward.normalized,enemyVector.normalized);

            // If calculated angle is less than the minimum lock angle, and is less than last enemy's calculated dot product.
            if(Mathf.Acos(dotResult) * Mathf.Rad2Deg < lockAngleRange && dotResult > selectedDotResult)
            {
                selectedDotResult = dotResult;
                selectedEnemy = i;
            }
        }

        // After checked thought all the enemys and found at least one enemy, and it is in the range, then set target to that enemy.
        if(selectedEnemy != -1 && (enemys[selectedEnemy].GetComponent<EnemyCore>().this_lockOnTarget.position - transform.position).magnitude < lockMinDistance)
        {
            targetTransform = enemys[selectedEnemy].GetComponent<EnemyCore>().this_lockOnTarget;
            isLockingOnTarget = true;
            CM_LockOnTarget.m_YAxis.Value = CM_ThirdPerson.m_YAxis.Value;
        }
    }
}
