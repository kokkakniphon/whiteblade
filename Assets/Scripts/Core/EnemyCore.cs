﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
public enum EnemyType
{
    normalClose,
    normalFar,
    spinClose
}

[RequireComponent(typeof(NavMeshAgent),typeof(Animator))]
public class EnemyCore : CharacterStats
{

    [Header("General Setting")]
    public Transform this_lockOnTarget;
    public EnemyType enemyType;


    [Header("Movement Setting")]
    public float lookAngle = 45f;
    public float lookRadius = 10f;
    public float rotationSpeed = 5f;

    [Header("Attack Setting")]
    public float attackDelay = 2f;
    public float attackCooldown = 2f;
    public float attackDistanceCooldown = 8f;
    float attackCurrentCooldown;
    public float attackCurrentDistanceCooldown;
    public bool foundPlayer;
    public bool attackOnceDelay;
    float distance;

    public GameObject lastTarget;

    private Vector3 lastTargetPosition;
    [HideInInspector] public Vector3 lastSeenTarget;
    [HideInInspector] public NavMeshAgent agent;
    [HideInInspector] public Animator anim;
 
    [Header("Attack Moveset")]
    public List<EnemyAttackPhase> phasesClose = new List<EnemyAttackPhase>();
    public List<EnemyAttackPhase> phasesFar = new List<EnemyAttackPhase>();
    [HideInInspector] public int currentPhase;
    [HideInInspector] public int currentMoveSet;
     public int currentMove = -1;
    [SerializeField] private float currentAnimationDuration = -1;
    [SerializeField] private List<float> allAttackAnimationDuration = new List<float>();
    public GameObject thisRoot;

    [Header("Sound Setting")]
    public SoundAssetPlayer walkingSound;
    public SoundAssetPlayer shootingSound;
    public SoundAssetPlayer attackingSound;

    public void OnEnable() 
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        SetFoundPlayer(false);
        takedDamage += GetHitted;
    }

    public override void Start()
    {
        base.Start();
        

        GetAttacksDuration();
        currentMove = -1;
    }

    public virtual void OnDestroy() 
    {
        takedDamage -= GetHitted;
    }
    public void GetAttacksDuration()
    {
        AnimationClip[] clips = anim.runtimeAnimatorController.animationClips;
        for (int i = 0; i < clips.Length; i++)
        {
            string attackName = "Attack" + i.ToString();
            for (int j = 0; j < clips.Length; j++)
            {
                if(clips[j].name == attackName)
                {
                    allAttackAnimationDuration.Add(clips[j].length);
                }
            }
        }
    }

    public override void Update() 
    {
        if(GM.instance == null || GM.instance.player == null)
            return;

        base.Update();
        if(!isDead)
        {
            AnimationManager();

            // Cool down
            if(attackCurrentCooldown > 0)
            {
                attackCurrentCooldown -= Time.deltaTime;
            }

            if(attackCurrentDistanceCooldown > 0)
            {
                attackCurrentDistanceCooldown -= Time.deltaTime;
            }

            distance = Vector3.Distance(GM.instance.player.transform.position, transform.position);

            if (distance <= lookRadius || foundPlayer)
            {
                float angle = Mathf.Acos(Vector3.Dot((GM.instance.player.transform.position - transform.position).normalized, transform.forward)) * Mathf.Rad2Deg;
                if(angle < lookAngle || foundPlayer)
                {   
                    Movement();

                    if (!foundPlayer)
                    {
                        SetFoundPlayer(true);
                    }

                    if(attackCurrentCooldown <= 0)
                    {
                        if (distance <= agent.stoppingDistance && !isAttacking && enemyType == EnemyType.normalClose)
                        {
                            isAttackClose = true;
                            AttackStage();
                            FaceTarget();
                        }
                        else if(distance <= agent.stoppingDistance && attackCurrentDistanceCooldown <= 0 && enemyType == EnemyType.normalFar)
                        {
                            attackCurrentDistanceCooldown = attackDistanceCooldown;
                            isAttackClose = false;
                            AttackStage();
                            FaceTarget();
                        }
                        else if (distance <= agent.stoppingDistance && !isAttacking && enemyType == EnemyType.spinClose)
                        {
                            isAttackClose = true;
                            AttackStage();
                            FaceTarget();
                        }
                    }
                }
            }

            if(currentAnimationDuration > 0 || isAttacking && !isAttackClose && currentMove > 0)
            {
                currentAnimationDuration -= Time.deltaTime;
            }
            else if(currentMove >= 0)
            {
                AttackMove();
            }
            else if(isAttacking)
            {
                endAttacking();
                attackCurrentCooldown = attackCooldown;
                attackOnceDelay = false;
            }
        }
        else
        {
            agent.speed = 0;
        }
    }

    void GetHitted(GameObject obj)
    {
        if (!foundPlayer && obj == this.gameObject)
        {
            SetFoundPlayer(true);
        }
    }

    public void SetFoundPlayer(bool setBool)
    {
        foundPlayer = setBool;
        anim.SetFloat("C_FoundPlayer",(setBool) ? 1 : 0);
    }

    void AttackStage()
    {
        StartCoroutine(AttackWithCooldown());
    }

    IEnumerator AttackWithCooldown()
    {
        agent.velocity = Vector3.zero;
        if(!attackOnceDelay)
        {
            attackOnceDelay = true;
            yield return new WaitForSeconds(attackDelay);
            if(!isAttackClose)
            {
                lastSeenTarget = GM.instance.player.transform.position;
                lastSeenTarget += (GM.instance.player.transform.position - transform.position).normalized * 15f;
                Attack();
            }
            else
            {
                yield return new WaitUntil(() => distance <= agent.stoppingDistance);
                lastTargetPosition = (GM.instance.player.transform.position - transform.position).normalized;
                Attack();
            }
        }
    }
    public virtual void Attack()
    {
        // Setting the moveset
        if(isAttackClose)
        {
            if(phasesClose.Count > 0)
            {
                currentMoveSet = Random.Range(0,phasesClose[currentPhase].GetAttackSetCount());
                currentMove = phasesClose[currentPhase].GetAttackSet(currentMoveSet).GetAttackMoveCount() - 1;
            }
        }
        else
        {
            if(phasesFar.Count > 0)
            {
                currentMoveSet = Random.Range(0,phasesFar[currentPhase].GetAttackSetCount());
                currentMove = phasesFar[currentPhase].GetAttackSet(currentMoveSet).GetAttackMoveCount() - 1;
            }
        }
        
        isAttacking = true;
        AttackMove();
    }

    void AttackMove()
    {
        if(isAttackClose)
        {
            if(phasesClose.Count > 0)
            {
                anim.SetFloat("S_Attack_Set", phasesClose[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSetNumber());
                currentAnimationDuration = allAttackAnimationDuration[phasesClose[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSetNumber()] / phasesClose[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSpeed();
            }
        }
        else
        {
            if(phasesFar.Count > 0)
            {
                if(phasesFar[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSetNumber() == 3)
                {
                    lastSeenTarget = GM.instance.player.transform.position;
                    lastSeenTarget += (GM.instance.player.transform.position - transform.position).normalized * 15f;
                }
                anim.SetFloat("S_Attack_Set", phasesFar[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSetNumber());
                currentAnimationDuration = allAttackAnimationDuration[phasesFar[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSetNumber()] / phasesFar[currentPhase].GetAttackSet(currentMoveSet).GetAttackMove(currentMove).GetAttackSpeed();
            }
        }

        anim.SetTrigger("S_Attack");
        --currentMove;
    }

    void AnimationManager()
    {
        if(agent != null)
        {
            if(isAttacking)
                anim.SetFloat("C_Speed",0);
            else
                anim.SetFloat("C_Speed",agent.velocity.magnitude/agent.speed);

            anim.SetBool("isAttacking",isAttacking);
        }
    }

    public void WalkSFX()
    {
        walkingSound.PlayAt(this.gameObject);
    }

    public void AttackSFX()
    {
        if(phasesClose.Count > 0)
            attackingSound.PlayAt(this.gameObject);
        if(phasesFar.Count > 0)
            shootingSound.PlayAt(this.gameObject);
    }
    public virtual void Movement()
    {
        if(agent != null && !isAttacking && !isDead)
        {
            if(enemyType == EnemyType.spinClose)
            {
                agent.speed = 15f;
                agent.acceleration = 15f;
            }
            else if(enemyType == EnemyType.normalClose)
            {
                agent.speed = 8f;
                agent.acceleration = 15f;
            }
            else if(enemyType == EnemyType.normalFar)
            {
                agent.speed = 3.5f;
                agent.acceleration = 8f;
                FaceTarget();
            }
            agent.SetDestination(GM.instance.player.transform.position);
        }

        if(isAttacking && enemyType == EnemyType.spinClose)
        {
            agent.speed = 8f;
            agent.acceleration = 8f;
            agent.SetDestination(lastTargetPosition * 20f + transform.position);
        }
        
    }

    public override void Die()
    {
        base.Die();
        anim.SetTrigger("C_Dead");
    }

    void FaceTarget()
    {
        // if(!isAttacking)
        // {
            Vector3 direction = (GM.instance.player.transform.position - transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * rotationSpeed);
        // }
    }

    private void OnDrawGizmosSelected() 
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);    
    }
}

[System.Serializable]
public class EnemyAttackPhase
{
    [SerializeField] private List<EnemyAttackSet> sets = new List<EnemyAttackSet>();
    public EnemyAttackSet GetAttackSet(int set)
    {
        return sets[set];
    }
    public int GetAttackSetCount()
    {
        return sets.Count;
    }
}

[System.Serializable]
public class EnemyAttackSet
{
    [SerializeField] private List<EnemyAttactMove> moves = new List<EnemyAttactMove>();
    public EnemyAttactMove GetAttackMove(int move)
    {
        return moves[move];
    }
    public int GetAttackMoveCount()
    {
        return moves.Count;
    }
}

[System.Serializable]
public class EnemyAttactMove
{
    [SerializeField] private int attackSetNumber;
    [SerializeField] private float attackSpeed;
    public int GetAttackSetNumber()
    {
        return attackSetNumber;
    }
    public float GetAttackSpeed()
    {
        return attackSpeed;
    }
}
