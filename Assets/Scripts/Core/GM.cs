﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Audio;

public class GM : MonoBehaviour
{
    public static GM instance;
	public MainMenu menu;
    public AudioMixer mainMixer;

    public GameObject player;
    public GameObject loadingScreen;
    public float mouseSensitivity;

    public bool isChangingLevel;
    public int currentLevel = -1;

    public List<GameObject> levels;
    public GameObject bossRoom;
    LevelController currentLevelController;
    BossRoom currentBossRoom;

    public delegate void noArgs();
    public static noArgs onFinishLoading;

    private void OnEnable() 
    {
        DontDestroyOnLoad(this.gameObject);
    }
    private void Awake() 
    {
        if(instance != null)
            Destroy(this.gameObject);
        else
            instance = this;
    }

    private void Start() 
    {
        if(player == null)
        {
            if(SceneManager.GetActiveScene().name == "FirstFloor" || SceneManager.GetActiveScene().name == "Tutorial")
                player = GameObject.FindGameObjectWithTag("Player");
        }  
    }

    private void Update() 
    {
        if(player == null)
        {
            if(SceneManager.GetActiveScene().name == "FirstFloor" || SceneManager.GetActiveScene().name == "Tutorial")
                player = GameObject.FindGameObjectWithTag("Player");
        }

        if(PlayerPrefs.GetInt("currentLevel") != currentLevel)
            currentLevel = PlayerPrefs.GetInt("currentLevel");
    }

    public void ChangeToNextLevel()
    {
        isChangingLevel = true;
        if(SceneManager.GetActiveScene().name == "FirstFloor")
        {
            if(PlayerPrefs.GetInt("currentLevel") < levels.Count-1)
            {
                // Normal level
                PlayerPrefs.SetInt("currentLevel",PlayerPrefs.GetInt("currentLevel")+1);
                StartCoroutine(ChangeLevel());
            }
            else if(currentBossRoom == null)
            {
                // Boss room
                StartCoroutine(LoadBossRoom());
            }
            else
            {
                PlayerPrefs.SetInt("currentLevel", 0);
                FakeLodeToScene("YouWin",2f);
            }
        }
        else if(SceneManager.GetActiveScene().name == "Tutorial")
        {
            player = null;
            PlayerPrefs.SetInt("currentLevel", 0);
            StartCoroutine(LoadFirstLevel());
        }
        else if(SceneManager.GetActiveScene().name == "Menu")
        {
            player = null;
            StartCoroutine(LoadFirstLevel());
        }
    }

    public void BossDead()
    {
        currentBossRoom.BossDead();
    }

    IEnumerator LoadBossRoom()
    {
        FakeLodeToScene();
        yield return new WaitForSeconds(3f);
        Destroy(currentLevelController.gameObject);
        // currentLevelController.Clear();
        // currentLevelController.gameObject.SetActive(false);
        currentBossRoom = Instantiate(bossRoom).GetComponent<BossRoom>();
        currentBossRoom.transform.position = Vector3.zero;
        currentBossRoom.SetUpRoom();
        player.transform.position = Vector3.up;
        isChangingLevel = false;
        CameraController.instance.CM_ThirdPerson.gameObject.SetActive(true);
    }

    IEnumerator LoadFirstLevel()
    {
        float currentLevelNum;
        mainMixer.GetFloat("MasterVolume", out currentLevelNum);
        yield return new WaitForSeconds(0);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        SceneManager.LoadScene("FirstFloor");
        yield return new WaitForSeconds(0.2f);
        if(onFinishLoading != null)
            onFinishLoading.Invoke();
        currentLevelController = Instantiate(levels[PlayerPrefs.GetInt("currentLevel")]).GetComponent<LevelController>();
        currentLevelController.StartGenerate();
        player = GameObject.FindGameObjectWithTag("Player");
        player.transform.position = Vector3.up;
        isChangingLevel = false;
        CameraController.instance.CM_ThirdPerson.gameObject.SetActive(true);
        StartCoroutine(FadingAudio(mainMixer, currentLevelNum, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        loadingScreen.SetActive(false);
        // End loading
    }

    IEnumerator ChangeLevel()
    {
        float currentLevelNum;
        mainMixer.GetFloat("MasterVolume", out currentLevelNum);
        yield return new WaitForSeconds(0);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        yield return new WaitForSeconds(0.3f);
        if(PlayerPrefs.GetInt("currentLevel") > 0)
        {
            currentLevelController.Clear();
            currentLevelController.gameObject.SetActive(false);
        }
        currentLevelController = Instantiate(levels[PlayerPrefs.GetInt("currentLevel")]).GetComponent<LevelController>();
        currentLevelController.StartGenerate();
        player.transform.position = Vector3.up;
        isChangingLevel = false;
        CameraController.instance.CM_ThirdPerson.gameObject.SetActive(true);
        StartCoroutine(FadingAudio(mainMixer, currentLevelNum, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        loadingScreen.SetActive(false);
        // End loading
    }

    public IEnumerator FadingAudio(AudioMixer mixer,float targetValue, float fadingDuration, int stepAmount)
    {
        float currentValue;
        mixer.GetFloat("MasterVolume", out currentValue);

        float lerpingValue = (targetValue - currentValue)/stepAmount;
        for (int i = 0; i < stepAmount; i++)
        {
            currentValue += lerpingValue;
            mixer.SetFloat("MasterVolume", currentValue);
            yield return new WaitForSeconds(fadingDuration/stepAmount);
        }
        mixer.SetFloat("MasterVolume", targetValue);
    }

    public void FakeLodeToScene()
    {
        StartCoroutine(fakeLoadingScreen());
    }
    public void FakeLodeToScene(string targetScene)
    {
        StartCoroutine(fakeLoadingScreen(targetScene,0));
    }
    public void FakeLodeToScene(string targetScene, float loadDealy)
    {
        StartCoroutine(fakeLoadingScreen(targetScene,loadDealy));
    }

    IEnumerator fakeLoadingScreen()
    {
        float currentLevel ;
        mainMixer.GetFloat("MasterVolume", out currentLevel);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        StartCoroutine(FadingAudio(mainMixer, currentLevel, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // End loading
        loadingScreen.SetActive(false);
        if(onFinishLoading != null)
            onFinishLoading.Invoke();
    }

    IEnumerator fakeLoadingScreen(string targetScene, float loadDealy)
    {
        float currentLevel ;
        mainMixer.GetFloat("MasterVolume", out currentLevel);
        yield return new WaitForSeconds(loadDealy);
        loadingScreen.SetActive(true);
        // Start loading
        StartCoroutine(FadingAudio(mainMixer, -80f, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // Completely loaded
        SceneManager.LoadScene(targetScene);
        StartCoroutine(FadingAudio(mainMixer, currentLevel, 2.5f, 100));
        yield return new WaitForSeconds(2.5f);
        // End loading
        loadingScreen.SetActive(false);
        if(onFinishLoading != null)
            onFinishLoading.Invoke();
    }


}
