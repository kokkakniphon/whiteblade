﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Cinemachine;

[RequireComponent(typeof(CharacterController))]
public class PlayerController : CharacterStats
{
    [Header ("General Setting")]
    public GameObject lookAtObject;
    public Image bloodSplatter;
    float bloodSplatterLerp;

    [Header ("Movement Setting")]
    [SerializeField] float walkMovementSpeed = 2f;
    [SerializeField] float runMovementSpeed = 2f;
    [SerializeField] float dodgeMovementSpeed = 11f;
    [SerializeField] float dodgeCoolDown = 1f;
    [SerializeField] float rotationSpeed = 0.1f;
    [SerializeField] float allowRotation = 0.1f;
    [SerializeField] float gravitationalScale = 5f;    
    [SerializeField] float dodgeDuration = 1f;
    [SerializeField] float dodgeCurrentDuration;
    [SerializeField] float skillDuration = 3f;
    float currentSkillDuration;
    float currentDodgeCoolDown;

    float speed;
    float sprint;
    float inputX, inputZ;
    float gravity;
    Camera cam;
    CharacterController characterController;
    Animator anim;
    public bool isDodgeing;
    public bool isUsingSkill;

    public static float currentSpeed;
    Vector3 desiredDiraction;
    Vector3 dodgeDiraction;

    [Header("Stamina Setting")]
    [SerializeField] float maxStamina = 100f;
    float currentStamina;
    [SerializeField] float runDecayRate = 1f;
    [SerializeField] float dodgeDecay = 20f;
    [SerializeField] float refillDelay = 1f;
    [SerializeField] float refillRate = 1f;
    public Image staminaBarFill;
    float currentRefillDelay;

    [Header("Skill Setting")]
    [SerializeField] float maxSkill = 100f;
    float currentSkillAmount;
    [SerializeField] float hitSkillAmount = 20f;
    [SerializeField] float skillRefillRate = 1.5f;
    public Image skillBarFill;
    public Image skillSelectionUI;
    public int currentSkillSelection = 0;
    public SoundAssetPlayer skillChangeSFX;
    public SoundAssetPlayer skillReadySFX;
    public SoundAssetPlayer skillNotReadySFX;

    [Header("Rapid Fire Skill")]
    public float rapidFireRate = 0.3f;
    public int rapidBulletAmount = 5;
    int currentRapidBulletAmount;
    public Sprite rapidUI;

    [Header("Explosive Bullet Skill")]
    public GameObject explosiveCharging;
    public GameObject explosiveBullet;
    [SerializeField] float explosiveDamage = 170;
    [SerializeField] float explosiveChargingDuration = 1f;
    public Sprite explosiveUI;

    public delegate void noArgs();
    public noArgs usedSkill;

    [Header("Shooting Setting")]
    public LayerMask shootableLayer;
    public Transform gunBarrel;
    [SerializeField] float shootingRate = 0.2f;
    float currentShootingCoolDown;
    [SerializeField] float bulletInitialSpeed = 10f;
    [SerializeField] Vector3 bulletOffset;
    [SerializeField] float regularBulletDamage = 34;
    [SerializeField] float currentBulletDamage;
    [SerializeField] int magazineSize = 7;
    [SerializeField] float reloadDuration = 2f;
    int currentAmmoAmount;
    float currentReloadDuration;
    public Image reloadFill;
    bool isReloading;
    public GameObject currentCanvas;
    public GameObject regularBullet;
    public GameObject currentBulletType;
    public GameObject crossBowOnHand;
    public GameObject crossBowOnHip;
    public TextMeshProUGUI ammoText;
    public TextMeshProUGUI lowAmmoText;
    public Animator hitEffect;

    [Header("Shooting SFX")]
    public SoundAssetPlayer regularSFX;
    public SoundAssetPlayer rapidSFX;
    public SoundAssetPlayer explosiveSFX;
    public SoundAssetPlayer emptySFX;
    public SoundAssetPlayer reloadSFX;
    public SoundAssetPlayer chargingSFX;

    public Vector3 bulletNoHitOffset;

    [Header("Potion")]
    public int healthPotionNum;
    public float healthRefillAmount;
    public TextMeshProUGUI healthPotionText;
    public Image healthPotionIcon;
    private bool isUsingPotion;
    public GameObject healthPotionEffect;
    public SoundAssetPlayer useItemSFX;
    public SoundAssetPlayer healSFX;

    private void Awake()
    {
        takedDamage += GetHitted;
    }

    private void OnDestroy() 
    {
        takedDamage -= GetHitted;
    }

    public override void Start()
    {
        base.Start();
        characterController = GetComponent<CharacterController>();
        cam = Camera.main;
        anim = GetComponent<Animator>();
        currentWeapon.SetTargetTag("Enemy");

        currentStamina = maxStamina;

        currentBulletType = regularBullet;
        // currentSkillAmount = 100f;

        currentBulletDamage = regularBulletDamage;
        currentAmmoAmount = magazineSize;
        SetAmmoUI();
        UpdatePotionVisual();
    }

    public override void Update()
    {
        if(currentCanvas.activeInHierarchy == Pause.GamePause)
            currentCanvas.SetActive(!Pause.GamePause);
        
        if(!isDead && !Pause.GamePause && !GM.instance.isChangingLevel)
        {
            // Get movement form Horizontal and Vertical axis.
            inputX = Input.GetAxis("Horizontal");
            inputZ = Input.GetAxis("Vertical");

            base.Update();
            // Smoothly lerp sprint to 1 if player pressing LeftShift and to 0 if player isn't.
            if(Input.GetKey(KeyCode.LeftShift) && currentStamina > 0 && !isUsingSkill && speed > 0 && !CameraController.instance.isADSing && !isUsingPotion)
            {
                sprint = Mathf.Lerp(sprint,1,Time.deltaTime*2);
                currentStamina -= runDecayRate * Time.deltaTime;
                currentRefillDelay = refillDelay;
            }
            else
            {
                sprint = Mathf.Lerp(sprint,0,Time.deltaTime*2);
            }

            if(dodgeCurrentDuration > 0)
            {
                dodgeCurrentDuration -= Time.deltaTime;
                desiredDiraction = dodgeDiraction;
            }
            else if(isDodgeing)
            {
                currentDodgeCoolDown = dodgeCoolDown;
                isDodgeing = false;
            }

            if(currentDodgeCoolDown > 0)
            {
                currentDodgeCoolDown -= Time.deltaTime;
            }

            // Refill Skill
            if(currentSkillAmount < maxSkill)
            {
                currentSkillAmount += Time.deltaTime * skillRefillRate;
            }
            else if(currentSkillAmount > maxSkill)
            {
                currentSkillAmount = maxSkill;
                skillReadySFX.Play();
            }

            if(!isAttacking)
            {
                InputDecider();
                MovementController();
            }

            // Left Click to Attack
            if (Input.GetMouseButtonDown(0) && !isDodgeing && !isUsingSkill && !CameraController.instance.isADSing)
            {
                Attack();
            }
            else if(Input.GetMouseButton(0) && CameraController.instance.isADSing && !isDodgeing && !isUsingSkill && currentReloadDuration <= 0)
            {
                anim.SetBool("isShooting",true);
                if(currentShootingCoolDown <= 0)
                {
                    currentShootingCoolDown = shootingRate;
                    Shoot();
                }
                else
                {
                    currentShootingCoolDown -= Time.deltaTime;
                }
            }
            else
            {
                currentShootingCoolDown -= Time.deltaTime;
                anim.SetBool("isShooting",false);
            }


            if (Input.GetKeyDown(KeyCode.R) && !isReloading && currentAmmoAmount != magazineSize)
            {
                currentReloadDuration = reloadDuration;
                isReloading = true;
                reloadSFX.Play();
            }

            if(currentReloadDuration > 0)
            {
                reloadFill.gameObject.SetActive(true);
                reloadFill.fillAmount = (reloadDuration-currentReloadDuration)/reloadDuration;
                currentReloadDuration -= Time.deltaTime;
            }
            else if(isReloading)
            {
                isReloading = false;
                reloadFill.gameObject.SetActive(false);
                currentAmmoAmount = magazineSize;
                SetAmmoUI();
            }

            if (currentRapidBulletAmount > 0 && currentShootingCoolDown <= 0)
            {
                currentRapidBulletAmount--;
                currentShootingCoolDown = rapidFireRate;
                Shoot();
                if(currentRapidBulletAmount == 0)
                {
                    isUsingSkill = false;
                }
            }

            // Do dodge
            if (Input.GetKeyDown(KeyCode.Space) && currentStamina > dodgeDecay && !isUsingSkill && currentDodgeCoolDown <= 0)
            {
                Dodge();
            }

            if (Input.GetKeyDown(KeyCode.Q))
            {
                currentSkillSelection = (currentSkillSelection+1) % 2;
                if(currentSkillSelection == 0)
                    skillSelectionUI.sprite = rapidUI;
                else
                    skillSelectionUI.sprite = explosiveUI;
                skillChangeSFX.Play();
            }

            // Use Skill
            if (Input.GetKeyDown(KeyCode.F) && CameraController.instance.isADSing && !isReloading)
            {
                if(currentSkillAmount >= maxSkill)
                {
                    // anim.SetTrigger("skill");
                    isUsingSkill = true;
                    if(currentSkillSelection == 0) //Rapid fire 5 bullet.
                    {
                        currentRapidBulletAmount = rapidBulletAmount;
                    }
                    else if(currentSkillSelection == 1)
                    {
                        Shoot();
                    }
                    currentSkillAmount = 0f;
                }
                else
                {
                    skillNotReadySFX.Play();
                }
                
            }

            if(isUsingSkill && currentSkillSelection == 1)
                explosiveCharging.transform.rotation = CameraController.instance.CM_ThirdPerson.gameObject.transform.rotation;

            // ADSing
            if(Input.GetMouseButton(1) && !isDodgeing && !isUsingSkill && !isUsingPotion)
            {
                crossBowOnHand.SetActive(true);
                crossBowOnHip.SetActive(false);
                CameraController.instance.isADSing = true;
            }
            else if(!isUsingSkill)
            {
                crossBowOnHand.SetActive(false);
                crossBowOnHip.SetActive(true);
                CameraController.instance.isADSing = false;
            }

            // Use Potion
            if(Input.GetKeyDown(KeyCode.E) && healthPotionNum > 0 && !isDodgeing && !isUsingSkill)
            {
                UsePotion();
            }

            // Manage Stamina
            if(currentRefillDelay > 0)
            {
                currentRefillDelay -= Time.deltaTime;
            }
            else
            {
                if(currentStamina < maxStamina)
                {
                    currentStamina += Time.deltaTime * refillRate;
                }
            }

            staminaBarFill.fillAmount = currentStamina/maxStamina;
            skillBarFill.fillAmount = currentSkillAmount/maxSkill;

            // Blood Splatter
            if(bloodSplatterLerp > 0)
            {
                bloodSplatterLerp -= Time.deltaTime;
            }
            else
            {
                bloodSplatterLerp = 0;
                bloodSplatter.color = new Color (bloodSplatter.color.r, bloodSplatter.color.g, bloodSplatter.color.b, Mathf.Lerp(bloodSplatter.color.a, 0,Time.deltaTime));
            }

        }
        else if(!GM.instance.isChangingLevel)
        {
            if(inputX > 0)
                inputX -= Time.deltaTime;
            else
                inputX += Time.deltaTime;
            if(inputZ > 0)
                inputZ -= Time.deltaTime;
            else
                inputZ += Time.deltaTime;
            InputDecider();
            MovementController();

            if(CameraController.instance.allowCameraControl && isDead)
                CameraController.instance.allowCameraControl = false;
        }
        else
        {
            sprint = 0f;
            isDodgeing = false;
            crossBowOnHand.SetActive(false);
            crossBowOnHip.SetActive(true);
            CameraController.instance.isADSing = false;
            InputDecider();
            MovementController();
        }
        AnimationController();
    }

    void Attack()
    {
        if(anim.GetBool("isAttack"))
        {
            anim.SetBool("isCombo", true);
        }
        else
        {
            currentWeapon.SetDamage(damage.GetValue());
            anim.SetTrigger("attack");
        }
    }

    void Hitted(bool isHead, Bullet fromBullet)
    {
        // currentSkillAmount += hitSkillAmount;

        // if(currentSkillAmount > maxSkill)
        //     currentSkillAmount = maxSkill;

        if(isHead)
            hitEffect.SetTrigger("hitHead");
        else
            hitEffect.SetTrigger("hitBody");

        fromBullet.hitted -= Hitted;
    }

    void GetHitted(GameObject thisObj)
    {
        if(thisObj == this.gameObject)
        {
            bloodSplatterLerp = 2f;
            bloodSplatter.color = new Color (bloodSplatter.color.r, bloodSplatter.color.g, bloodSplatter.color.b, bloodSplatter.color.a + 0.3f);
            this.GetComponent<CinemachineImpulseSource>().GenerateImpulse();
        }
    }

    public void ForceInput(Vector2 dir)
    {
        inputX = dir.x;
        inputZ = dir.y;
    }

    void UsePotion()
    {
        anim.SetTrigger("usePotion");
        useItemSFX.Play();
        healthPotionNum--;
        StartCoroutine(HealDelay());
        Destroy(Instantiate(healthPotionEffect, this.transform.position, Quaternion.identity, this.transform),10f);
        UpdatePotionVisual();
    }

    IEnumerator HealDelay()
    {
        yield return new WaitForSeconds(2f);
        base.GainHealth(healthRefillAmount);
        healSFX.Play();
    }

    public void RecievePotion()
    {
        healthPotionNum++;
        UpdatePotionVisual();
    }

    void UpdatePotionVisual()
    {
        healthPotionText.text = healthPotionNum.ToString();
        if(healthPotionNum > 0)
        {
            healthPotionIcon.gameObject.SetActive(true);
            healthPotionText.gameObject.SetActive(true);
        }
        else
        {
            healthPotionIcon.gameObject.SetActive(false);
            healthPotionText.gameObject.SetActive(false);
        }
    }

    void Dodge()
    {
        Vector3 targetForward;
        Vector3 targetRight;

        targetForward = cam.transform.forward;
        targetRight = cam.transform.right;

        targetForward.y = 0;
        targetRight.y = 0;

        targetForward.Normalize();
        targetRight.Normalize();

        inputX = Input.GetAxisRaw("Horizontal");
        inputZ = Input.GetAxisRaw("Vertical");

        desiredDiraction = targetForward * inputZ + targetRight * inputX;

        if(desiredDiraction != Vector3.zero && !isDodgeing && !isAttacking)
        {
            anim.SetTrigger("dodge");
            isDodgeing = true;
            desiredDiraction.Normalize();
            dodgeDiraction = desiredDiraction;
            dodgeCurrentDuration = dodgeDuration;
            currentStamina -= dodgeDecay;
            currentRefillDelay = refillDelay;
        }
    }

    void SetAmmoUI()
    {
        ammoText.text = currentAmmoAmount + "/" + magazineSize;
        lowAmmoText.gameObject.SetActive(currentAmmoAmount < 1/3f * magazineSize);
    }

    void Shoot()
    {
        if(!isUsingSkill)
        {
            if(currentAmmoAmount > 0)
            {
                currentAmmoAmount--;
            }
            else
            {
                emptySFX.Play();
                return;
            }
        }
        else if(currentSkillSelection == 1)
        {
            StartCoroutine(skillExplosiveCharging());
            return;
        }

        SetAmmoUI();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Rigidbody bulletRD = Instantiate(currentBulletType,gunBarrel.gameObject.transform.position,CameraController.instance.CM_ThirdPerson.gameObject.transform.rotation).GetComponent<Rigidbody>();
        bulletRD.gameObject.GetComponent<Bullet>().SetStat(currentBulletDamage,true,"Enemy",this.tag,true);
        bulletRD.gameObject.GetComponent<Bullet>().hitted += Hitted;
        if (Physics.Raycast(ray, out hit, 100, shootableLayer))
        {
            bulletRD.AddForce((hit.point - bulletRD.transform.position).normalized * bulletInitialSpeed);
        }
        else
        {
            bulletRD.AddForce((transform.forward + transform.right * bulletNoHitOffset.x + transform.up * bulletNoHitOffset.y).normalized * bulletInitialSpeed);
        }

        if(isUsingSkill)
        {
            if(currentSkillSelection == 0)
            {
                rapidSFX.Play();
            }
            else if(currentSkillSelection == 1)
            {
                explosiveSFX.Play();
            }
        }
        else
        {
            regularSFX.Play();
        }

        Destroy(bulletRD.gameObject,3f);

    }

    IEnumerator skillExplosiveCharging()
    {
        currentBulletDamage = explosiveDamage;
        currentBulletType = explosiveBullet;

        // Destroy(Instantiate(explosiveCharging,gunBarrel.transform.position,gunBarrel.transform.rotation,gunBarrel),1f);
        explosiveCharging.SetActive(true);
        chargingSFX.Play();
        yield return new WaitForSeconds(explosiveChargingDuration);
        explosiveCharging.SetActive(false);
        
        SetAmmoUI();
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        Rigidbody bulletRD = Instantiate(currentBulletType,gunBarrel.gameObject.transform.position,CameraController.instance.CM_ThirdPerson.gameObject.transform.rotation).GetComponent<Rigidbody>();
        bulletRD.gameObject.GetComponent<Bullet>().SetStat(currentBulletDamage,true,"Enemy",this.tag, false);
        if (Physics.Raycast(ray, out hit, 100))
        {
            bulletRD.AddForce((hit.point - bulletRD.transform.position).normalized * bulletInitialSpeed);
        }
        else
        {
            bulletRD.AddForce((-CameraController.instance.CM_ADSThirdPerson.gameObject.transform.forward  + bulletOffset) * bulletInitialSpeed);
        }

        if(isUsingSkill)
        {
            if(currentSkillSelection == 0)
            {
                rapidSFX.Play();
            }
            else if(currentSkillSelection == 1)
            {
                explosiveSFX.Play();
            }
        }
        else
        {
            regularSFX.Play();
        }

        Destroy(bulletRD.gameObject,3f);

        currentBulletType = regularBullet;
        currentBulletDamage = regularBulletDamage;
        isUsingSkill = false;
    }

    void InputDecider()
    {
        // Convert X and Y input to speed by using Trigonometry. 
        if(!Pause.GamePause)
        {
            if(inputX != 0 && inputZ != 0)
            {
                inputX /= 1.44f;
                inputZ /= 1.44f;
            }
        }
        speed = new Vector2(inputX, inputZ).magnitude;

        // If the speed is more than speed require to rotate OR player is locking on the target
        if(speed > allowRotation || CameraController.instance.isLockingOnTarget || isDodgeing)
        {
            RotationController();
        }
        else if(CameraController.instance.isADSing)
        {
            Vector3 targetForward = -CameraController.instance.CM_ADSThirdPerson.transform.forward.normalized;
            targetForward.y = 0;
            // Make player look at (face forward target).
            lookAtObject.transform.LookAt(targetForward);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetForward), rotationSpeed * Time.deltaTime);
            desiredDiraction = Vector3.zero;
        }
        else if(!isDodgeing)
        {
            desiredDiraction = Vector3.zero;
        }
    }

    void RotationController()
    {

        Vector3 targetForward;
        Vector3 targetRight;

        // If player is locking at target set desired direction according to the target position, else set desired direction according to camera forward vector.
        if(isDodgeing)
        {
            desiredDiraction = dodgeDiraction;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredDiraction), rotationSpeed * Time.deltaTime);
        }
        else if(CameraController.instance.isLockingOnTarget)
        {
            Vector3 targetPosition = CameraController.instance.targetTransform.position;
            targetPosition.y = transform.position.y;

            targetForward = (targetPosition - transform.position).normalized;
            targetRight = transform.right.normalized;

            targetForward.y = 0;
            targetRight.y = 0;

            desiredDiraction = targetForward * inputZ + targetRight * inputX;

            // Make player look at (face forward target).
            lookAtObject.transform.LookAt(targetPosition);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation((targetPosition - transform.position).normalized), rotationSpeed * Time.deltaTime);
        }
        else if(CameraController.instance.isADSing)
        {
            targetForward = -CameraController.instance.CM_ADSThirdPerson.transform.forward.normalized;
            targetRight = transform.right.normalized;

            targetForward.y = 0;
            targetRight.y = 0;

            desiredDiraction = targetForward * inputZ + targetRight * inputX;

            // Make player look at (face forward target).
            lookAtObject.transform.LookAt(targetForward);

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetForward), rotationSpeed * Time.deltaTime);
        }
        else
        {
            targetForward = cam.transform.forward;
            targetRight = cam.transform.right;

            targetForward.y = 0;
            targetRight.y = 0;

            targetForward.Normalize();
            targetRight.Normalize();

            desiredDiraction = targetForward * inputZ + targetRight * inputX;

            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(desiredDiraction), rotationSpeed * Time.deltaTime);
        }

    }

    void MovementController()
    {
        gravity -= gravitationalScale * Time.deltaTime;

        if(!isDodgeing)
        {
            // Set movement speed according to sprint value.
            currentSpeed = (sprint > 0.5f) ? runMovementSpeed : walkMovementSpeed;
        }
        else
        {
            currentSpeed = dodgeMovementSpeed;
        }
        // Move player in the direction of the desired direction 
        Vector3 moveDir = desiredDiraction * Time.deltaTime * currentSpeed;
        moveDir = new Vector3(moveDir.x, gravity, moveDir.z);
        characterController.Move(moveDir);

        if(characterController.isGrounded)
        {
            gravity = 0;
        }
    }

	public override void Die()
    {
        base.Die();
        if(anim != null)
            anim.SetTrigger("C_Dead");
    }

    public override void startAttacking()
    {
        if(anim != null)
            anim.SetBool("isAttack", true);
        base.startAttacking();
    }

    public override void endAttacking()
    {
        if(anim != null)
            anim.SetBool("isAttack", false);
        base.endAttacking();
    }

    public override void startInAttackArea()
    {
        base.startInAttackArea();
    }

    public override void endInAttackArea()
    {
        base.endInAttackArea();
    }

    public void endCombo()
    {
        if(anim != null)
            anim.SetBool("isCombo", false);
    }

    void AnimationController()
    {
        anim.SetBool("isLockOn",CameraController.instance.isADSing);
        anim.SetFloat("Speed",speed);
        anim.SetFloat("InputX",inputX);
        anim.SetFloat("InputZ",inputZ);
        anim.SetFloat("Sprint",sprint);

    }

}
