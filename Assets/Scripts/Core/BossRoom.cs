﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BossRoom : MonoBehaviour
{
    public Animator door;
    public BigBossEnemy bossEnemy;
    public void SetUpRoom()
    {
        door.SetBool("isLock", true);
        bossEnemy.foundPlayer = true;
        this.GetComponent<NavMeshSurface>().BuildNavMesh();
    }

    public void BossDead()
    {
        door.SetBool("isLock", false);
    }
}
