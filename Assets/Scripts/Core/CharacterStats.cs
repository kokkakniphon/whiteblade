﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class CharacterStats : MonoBehaviour
{

    [Header("Stats")]
    public WeaponCore currentWeapon;
    public float maxHealth = 100;
    [SerializeField] public float currentHealth {get; private set;}
    public Stat damage;
    public bool hitted {get; private set;}
    public float invencibleDuration = 0.5f;
    public bool isInAttackingArea {get; private set;}
    public bool isAttacking;
    public bool isAttackClose;
    public bool isDead;
    [HideInInspector] public float invencibleCurrentDuration;
    [Header("Health Setting")]
    public bool autoHideHealthBar = true;
    public float healthDisplayDuration;
    private float currentHealthDuration;
    public GameObject healthBar;
    public Image healthFill;
    public Image healthFillLerp;
    [Header("Sound")]
    public SoundAsset gotHitBodySFX;
    public SoundAsset gotHitHeadSFX;

    public delegate void GameObjectArgs(GameObject obj);
    public static GameObjectArgs takedDamage;
    public GameObjectArgs triggerDead;


    public virtual void Start() 
    {
        currentHealth = maxHealth;
        if(healthBar != null)
        {
            healthFill.fillAmount = 1f;
            if (healthFillLerp != null)
                healthFillLerp.fillAmount = 1f;
            if(!autoHideHealthBar)
                healthBar.SetActive(true);
        }
        
    }

    public virtual void TakeDamage(float damage)
    {
        invencibleCurrentDuration = invencibleDuration;
        currentHealthDuration = healthDisplayDuration;
        currentHealth -= damage;
        if(healthBar != null)
            healthFill.fillAmount = currentHealth/maxHealth;
        
        // Call take damage
        if(takedDamage != null)
            takedDamage.Invoke(this.gameObject);

        if(currentHealth <= 0)
        {
            currentHealth = 0;
            Die();
        }
    }

    public virtual void Update() 
    {
        if(invencibleCurrentDuration > 0)
        {
            hitted = true;
            invencibleCurrentDuration -= Time.deltaTime;
        }
        else
        {
            hitted = false;
        }

        if(healthBar != null)
        {
            if(currentHealthDuration > 0)
            {
                if(!healthBar.activeInHierarchy && autoHideHealthBar)
                    healthBar.SetActive(true);
                currentHealthDuration -= Time.deltaTime;
            }
            else
            {
                if(healthBar.activeInHierarchy && autoHideHealthBar)
                    healthBar.SetActive(false);
            }

            if(healthFillLerp != null)
                healthFillLerp.fillAmount = Mathf.Lerp(healthFillLerp.fillAmount,currentHealth/maxHealth,Time.deltaTime);
        }
    }

    public virtual void GainHealth(float amountFloat)
    {
        currentHealth += amountFloat;
        if(healthBar != null)
            healthFill.fillAmount = currentHealth/maxHealth;
    }

    public virtual void Die()
    {
        if(isDead)
            return;
        endInAttackArea();
        endAttacking();

        if(triggerDead != null)
            triggerDead.Invoke(this.gameObject);

        if(this.gameObject.tag != "Player")
            this.GetComponent<Collider>().isTrigger = true;
        // Die
        isDead = true;
        if(this.gameObject.tag == "Player")
        {
            GM.instance.FakeLodeToScene("GameOver",3f);
            Destroy(this.gameObject,7f);
        }
        else
        {
            Destroy(this.gameObject,4f);
        }
    }

    public virtual void startAttacking()
    {
        isAttacking = true;
    }

    public virtual void endAttacking()
    {
        isAttacking = false;
    }

    public virtual void startInAttackArea()
    {
        if(currentWeapon != null)
            currentWeapon.SetAttack(true);
        isInAttackingArea = true;
    }

    public virtual void endInAttackArea()
    {
        if(currentWeapon != null)
            currentWeapon.SetAttack(false);
        isInAttackingArea = false;
    }

}


[System.Serializable]
public class Stat
{
    [SerializeField] private float baseValue;

    public float GetValue()
    {
        return baseValue;
    }
}
