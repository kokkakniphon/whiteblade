﻿ using UnityEngine;
 using UnityEditor;
 
 public class RoomTools : EditorWindow
 {
    private Object currentTile;
    private Vector3 prevPosition;
    private bool doSnap = false;
    private bool doAdd = false;
    private float snapValue = 5;

    [MenuItem( "Edit/RoomTools %_l" )]

    static void Init()
    {
        var window = (RoomTools)EditorWindow.GetWindow( typeof( RoomTools ) );
        window.maxSize = new Vector2( 200, 100 );
    }

    public void OnGUI()
    {
        GUILayout.Label("Base Settings", EditorStyles.boldLabel);
        doAdd = EditorGUILayout.Toggle( "ClickAdd/Delete", doAdd );
        currentTile = EditorGUILayout.ObjectField(currentTile, typeof(Object), true);

        GUILayout.Label("Auto Snap Setting", EditorStyles.boldLabel);
        doSnap = EditorGUILayout.Toggle( "Auto Snap", doSnap );
        snapValue = EditorGUILayout.FloatField( "Snap Value", snapValue );
        if(GUILayout.Button("Double"))
        {
            snapValue *= 2;
        }
        if(GUILayout.Button("Divide"))
        {
            snapValue /= 2;
        }
    }

    public void Update()
    {
        if ( doSnap
            && !EditorApplication.isPlaying
            && Selection.transforms.Length > 0
            && Selection.transforms[0].position != prevPosition )
        {
            Snap();
            prevPosition = Selection.transforms[0].position;
        }

        if(doAdd && Input.GetMouseButton(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            
            if (Physics.Raycast(ray, out hit)) {
                Transform objectHit = hit.transform;
                
                Debug.Log(hit.collider.name);
            }
        }
    }

    // void OnSceneGUI()
    // {
    //     if (Event.current.type == EventType.MouseMove)
    //     {
    //         RaycastHit hit;
    //         if(Physics.Raycast(HandleUtility.GUIPointToWorldRay(Event.current.mousePosition) , out hit))
    //         {
    //             Transform objectHit = hit.transform;
                
    //             Debug.Log(hit.collider.name);
    //         }
                
    //     }
    // }

    private void Snap()
    {
        foreach ( var transform in Selection.transforms )
        {
            var t = transform.transform.position;
            t.x = Round( t.x );
            t.y = Round( t.y );
            t.z = Round( t.z );
            transform.transform.position = t;
        }
    }

    private float Round( float input )
    {
        return snapValue * Mathf.Round( ( input / snapValue ) );
    }
 }