﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    // Start is called before the first frame update
    private void Start() {
        Cursor.lockState = CursorLockMode.None; 
        Cursor.visible = true;   
    }

    public void NewGame()
    {
        GM.instance.FakeLodeToScene("Tutorial");
    }
    public void StartGame()
    {
        GM.instance.ChangeToNextLevel();
    }
    public void QuitGame()
    {
        Application.Quit();
    }
	public void ReturnToMenu()
	{
        
		GM.instance.FakeLodeToScene("Menu");
		Time.timeScale = 1f;
	}
}
