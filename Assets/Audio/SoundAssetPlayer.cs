﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundAssetPlayer : MonoBehaviour
{
    public SoundAsset soundAsset;
    GameObject currentSound;
    public bool playOnAwake;
    public bool is3D;
    private void Start() 
    {
        if(playOnAwake)
        {
            if(is3D)
                PlayAt(this.gameObject);
            else
                Play();
        }
    }
    
    public void Play()
    {
        if(soundAsset == null)
            return;
        GameObject soundPlayer = new GameObject();
        AudioSource soundPlayerSource = soundPlayer.AddComponent<AudioSource>();
        soundPlayerSource.clip = soundAsset.sound;
        soundPlayerSource.outputAudioMixerGroup = soundAsset.audioMixerGroup;
        soundPlayerSource.volume = soundAsset.volume;
        soundPlayerSource.pitch = soundAsset.pitch;
        soundPlayerSource.Play();
        Destroy(soundPlayer, soundAsset.sound.length);
    }

    public void Play(SoundAsset soundAsset)
    {
        if(soundAsset == null)
            return;
        GameObject soundPlayer = new GameObject();
        AudioSource soundPlayerSource = soundPlayer.AddComponent<AudioSource>();
        soundPlayerSource.clip = soundAsset.sound;
        soundPlayerSource.outputAudioMixerGroup = soundAsset.audioMixerGroup;
        soundPlayerSource.volume = soundAsset.volume;
        soundPlayerSource.pitch = soundAsset.pitch;
        soundPlayerSource.Play();
        Destroy(soundPlayer, soundAsset.sound.length);
    }

    public void PlayAt(GameObject selectedGameObject)
    {
        if(soundAsset == null)
            return;
        GameObject soundPlayer = new GameObject();
        AudioSource soundPlayerSource = soundPlayer.AddComponent<AudioSource>();
        soundPlayerSource.clip = soundAsset.sound;
        soundPlayerSource.outputAudioMixerGroup = soundAsset.audioMixerGroup;
        soundPlayerSource.volume = soundAsset.volume;
        soundPlayerSource.pitch = soundAsset.pitch;
        soundPlayerSource.spatialBlend = 1f;
        soundPlayerSource.minDistance = soundAsset.minRange;
        soundPlayerSource.Play();
        soundPlayer.transform.parent = selectedGameObject.transform;
        soundPlayer.transform.localPosition = Vector3.zero;
        Destroy(soundPlayer, soundAsset.sound.length);
    }
}
