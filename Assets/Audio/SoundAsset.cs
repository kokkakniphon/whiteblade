﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[CreateAssetMenu(fileName = "SOUND", menuName = "SoundAsset/New Sound Asset", order = 1)]
public class SoundAsset : ScriptableObject
{
    public AudioClip sound;
    public AudioMixerGroup audioMixerGroup;
    [Range(0,1)]
    public float volume = 1f;
    [Range(-3,3)]
    public float pitch = 1f;
    [Header("3D Setting")]
    public float minRange = 1f;
}
